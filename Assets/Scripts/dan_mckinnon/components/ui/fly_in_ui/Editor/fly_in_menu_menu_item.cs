﻿// Dan's Jam Pack for Unity #1.
// ---
// by Dan Player McKinnon
// E-mail: dan@danmckinnon.net
// Created: February 2016
// Description:
//		This asset pack gives some miscellanous boilerplate functionality
//		that are very handy when prototyping and jamming out games.
// Liscence:  
//		This work is licensed under the Creative Commons Attribution 4.0 International License. 
//		To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/.

// Fly-in menu menus
// ---
// Handy way to add fly-in menus or pages to fly-in menus.  

using UnityEngine;
using System.Collections;
using UnityEditor;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEditor.Events;
public class fly_in_menu_menu_item {

	private static void create_tag(string new_tag){
    	SerializedObject tag_manager = new SerializedObject (AssetDatabase.LoadAllAssetsAtPath ("ProjectSettings/TagManager.asset")[0]);
    	SerializedProperty tag_properties = tag_manager.FindProperty("tags");
        bool found = false;

        int i = 0;
        for ( i = 0; !found && i < tag_properties.arraySize; i++) {
            found = found || tag_properties.GetArrayElementAtIndex(i).stringValue == new_tag;
        }
        
        if (!found) {
            tag_properties.InsertArrayElementAtIndex(i);
            tag_properties.GetArrayElementAtIndex(i).stringValue = new_tag;
        }

        tag_manager.ApplyModifiedProperties();
    }


    //
    // ---
    //
    //
    [MenuItem("GameObject/Dan McKinnon/UI/Fly-in menu system",false,10)]
    public static void add_fly_in_menu() {



        create_tag("MainMenu");
        GameObject main_menu = GameObject.FindGameObjectWithTag("MainMenu");

        if (main_menu != null) {
            if (!EditorUtility.DisplayDialog("Create another fly-in menu", "There is already a main menu in this scene.  Are you sure you would like to create another one?","Yes", "Cancel")) {
                return;
            }
            
        }

		//GameObject heirarchy...
		//menu 				- tag as main, add fly-in menu
		//	page1 			
		//		button1        
		//		button2		
		//		nav_page2 	- add Unity Event Linking to page 2
		//	page2
		//		button1
		//		button2
		//		back 		- add Unity Event linking back to page 1

		GameObject menu = new GameObject("menu");
        RectTransform menu_rect = menu.AddComponent<RectTransform>();
        menu_rect.localPosition = Vector3.zero;
		GameObject page1 = new GameObject("page1");
        page1.AddComponent<RectTransform>();
        GameObject page1_button1 = new GameObject("button");
		GameObject page1_button2 = new GameObject("button");
		GameObject page1_button3 = new GameObject("page2_button");
		GameObject page2 = new GameObject("page2");
        page2.AddComponent<RectTransform>();
        GameObject page2_button1 = new GameObject("button");
		GameObject page2_button2 = new GameObject("button");
		GameObject page2_button3 = new GameObject("back_button");

		page1.transform.SetParent(menu.transform);
		page1_button1.transform.SetParent( page1.transform );
		page1_button2.transform.SetParent( page1.transform );
		page1_button3.transform.SetParent( page1.transform );
		
		page2.transform.SetParent(menu.transform);
		page2_button1.transform.SetParent( page2.transform );
		page2_button2.transform.SetParent( page2.transform );
		page2_button3.transform.SetParent( page2.transform );


		menu.tag = "MainMenu";
        fly_in_menu fly_in = menu.AddComponent<fly_in_menu>();
        fly_in.default_menu = page1;

        Text text1 = page1_button1.AddComponent<Text>();
        text1.text = "Button1";        
        Text text2 = page1_button2.AddComponent<Text>();
        text2.text = "Button2";
        Text text3 = page1_button3.AddComponent<Text>();
        text3.text = "Page 2";
        Text text4 = page2_button1.AddComponent<Text>();
        text4.text = "Button4";
        Text text5 = page2_button2.AddComponent<Text>();
        text5.text = "Button5";
        Text text6 = page2_button3.AddComponent<Text>();
        text6.text = "Back";

        Button button1 = page1_button1.AddComponent<Button>();
        button1.targetGraphic = text1;
        Button button2 = page1_button2.AddComponent<Button>();
        button2.targetGraphic = text2;
        Button button3 = page1_button3.AddComponent<Button>();
        button3.targetGraphic = text3;
        Button button4 = page2_button1.AddComponent<Button>();
        button4.targetGraphic = text4;
        Button button5 = page2_button2.AddComponent<Button>();
        button5.targetGraphic = text5;
        Button button6 = page2_button3.AddComponent<Button>();
        button6.targetGraphic = text6;


        //position buttons
        page1_button1.transform.position = new Vector3(0, 20, 0);
        page1_button2.transform.position = new Vector3(0, 0, 0);
        page1_button3.transform.position = new Vector3(0, -20, 0);

        page2_button1.transform.position = new Vector3(0, 20, 0);
        page2_button2.transform.position = new Vector3(0, 0, 0);
        page2_button3.transform.position = new Vector3(0, -20, 0);

        page2.SetActive(false);


        //add events to buttons
        
        UnityEventTools.AddObjectPersistentListener(button3.onClick, new UnityAction<GameObject>(fly_in.open), page2);
        UnityEventTools.AddObjectPersistentListener(button6.onClick, new UnityAction<GameObject>(fly_in.open), page1);

        //parent new object
        if ( Selection.activeGameObject != null) {
            menu.transform.SetParent(Selection.activeGameObject.transform,false);
        }
        Undo.RegisterCreatedObjectUndo(menu,"create fly-in menu");

	}

    //
    // ---
    //
    //
    [MenuItem("GameObject/Dan McKinnon/UI/Add fly-in menu page",false,10)]
    public static void add_page() {
        GameObject main_menu = GameObject.FindGameObjectWithTag("MainMenu");
        if (main_menu == null) {
            if ( EditorUtility.DisplayDialog("Add fly-in menu?", "A fly-in menu could not be found,  would you like to add one?", "Yes", "No")) {
                add_fly_in_menu();
            }

        } else {
            GameObject page = new GameObject("new_page");
            page.AddComponent<RectTransform>();
            GameObject button1 = new GameObject("button1");
            GameObject button2 = new GameObject("button2");
            GameObject button3 = new GameObject("button3");


            Text t;

            t = button1.AddComponent<Text>();
            t.text = "Button1";
            button1.AddComponent<Button>().targetGraphic = t;
            t = button2.AddComponent<Text>();
            t.text = "Button2";
            button2.AddComponent<Button>().targetGraphic = t;
            t = button3.AddComponent<Text>();
            t.text = "Button3";
            button3.AddComponent<Button>().targetGraphic = t;

            button1.transform.position = new Vector3(0, 20, 0);
            button2.transform.position = new Vector3(0, 0, 0);
            button3.transform.position = new Vector3(0, -20, 0);

            button1.transform.SetParent(page.transform);
            button2.transform.SetParent(page.transform);
            button3.transform.SetParent(page.transform);


            page.transform.SetParent(main_menu.transform,false);

            Undo.RegisterCreatedObjectUndo(page, "add fly-in menu page");
        }
    }
}
