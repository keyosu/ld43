﻿// Dan's Jam Pack for Unity #1.
// ---
// by Dan Player McKinnon
// E-mail: dan@danmckinnon.net
// Created: February 2016
// Description:
//		This asset pack gives some miscellanous boilerplate functionality
//		that are very handy when prototyping and jamming out games.
// Liscence:  
//		This work is licensed under the Creative Commons Attribution 4.0 International License. 
//		To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/.

// Exit on escape
// ---
// Exit the program immediately when the esc key is pressed.

using UnityEngine;
using System.Collections;
[AddComponentMenu("Dan McKinnon/UI/Exit on escape key")]
public class exit_on_escape : MonoBehaviour {

	
    void OnGUI() {
        Event e = Event.current;
        if (e.isKey && e.keyCode == KeyCode.Escape ) {
            Application.Quit();
        }
    }
}
