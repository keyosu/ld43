﻿// Dan's Jam Pack for Unity #1.
// ---
// by Dan Player McKinnon
// E-mail: dan@danmckinnon.net
// Created: February 2016
// Description:
//		This asset pack gives some miscellanous boilerplate functionality
//		that are very handy when prototyping and jamming out games.
// Liscence:  
//		This work is licensed under the Creative Commons Attribution 4.0 International License. 
//		To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/.

// Chase camera
// ---
// An exciting orthographic camera that chases an object smoothly and zooms in/out as its speed increases / decreases
// It will never leave the boundaries,  however when zoomed out it may violate the boundaries when zoomed out as though
// it were not violating the boundaries when fully zoomed in.

using UnityEngine;
using System.Collections;
[AddComponentMenu("Dan McKinnon/Cameras/Chase camera")]
public class chase_camera : MonoBehaviour {

    [Tooltip("The offset the camera.  Note: changing the scale of the object being followed will modify the offset.  Great for maintaining 'the rule of thirds' and other framing techniques. ")]
    public Vector2 offset = new Vector2(4f, 1f);
	[Tooltip("A 'dead zone'.  The camera will not move if the target object is within this tolerance area. .")]
    public Vector2 tolerance = new Vector2(0, 1);
    [Tooltip("The distance the camera must be from the target in order to go max_speed.")]
    public float max_speed_distance = 2;
    [Tooltip("The maximum speed the camera can go.")]
    public float max_speed = 10;        //maximum speed the camera will move

    [Tooltip("The minimum orthographic size of the camera.   When an object is at rest the camera will zoom in to this value.")]
    public float min_size = 4;          //minimum (most zoom) orthographic projection size
    [Tooltip("The maximum size of the orthographic camera.  When the camera is moving at max_size_speed it will zoom to this value")]
    public float max_size = 16;         //maximum (least zoom) orthographic projection size
    [Tooltip("How fast the camera must be going to reach full zoom out.")]
    public float max_size_speed = 5;    //how fast the camera must be moving to be at max_size.
    [Tooltip("How quickly the zoom will change.")]
    public float zoom_speed = 4.0f;
    [Tooltip("An object to follow.  Note it's possible to change which object the camera follows at runtime and can be quite a nice effect for creating bosses or points of interest.")]
    public GameObject follow;       //what should the camera follow.
    [Tooltip("The boundaries the camera cannot move beyond")]
    public Rect boundaries = new Rect(-10,-10,20,20);               //a rectangular confined boundaries where the camera can go.

    private Camera cam;
    private Transform follow_transform;


	// Use this for initialization
	void Start () {
        cam = GetComponent<Camera>();
        follow_transform = follow.GetComponent<Transform>();
        Vector2 dest = get_dest(false);
        transform.position = new Vector3(dest.x, dest.y, transform.position.z);
        cam.orthographicSize = min_size;
    }

    public void change_target( GameObject target) {
        follow = target;
        follow_transform = target.transform;
    }
    
    // 
    // ---
    //
    public void maximize() {
        min_size = max_size;
        offset = Vector2.zero;

    }
    private Vector2 get_dest( bool do_tolerance) {
        float dest_x = follow_transform.position.x + (offset.x * follow_transform.localScale.x);
        float dest_y = follow_transform.position.y + (offset.y * follow_transform.localScale.y);
        if (do_tolerance) {

            if (Mathf.Abs(dest_x - transform.position.x) <= tolerance.x) {
                dest_x = transform.position.x;
            }
            if (Mathf.Abs(dest_y - transform.position.y) <= tolerance.y) {
                dest_y = transform.position.y;
            }
        }


        float w = (cam.orthographicSize / cam.pixelHeight) * cam.pixelWidth;
        float h = cam.orthographicSize;

        dest_x = Mathf.Clamp(dest_x, boundaries.xMin + w, boundaries.xMax - w);
        dest_y = Mathf.Clamp(dest_y, boundaries.yMin + h, boundaries.yMax - h);
        

        return new Vector2(dest_x, dest_y);

    }

    private float movement = 0;

	// Update is called once per frame

    void Update() {
        Vector2 dest2d = get_dest(true);
        //chase
        Vector3 dest = new Vector3(dest2d.x,dest2d.y, transform.position.z);
        float distance = Vector3.Distance(transform.position, dest);
        float speed = (distance / max_speed_distance) * max_speed;

        //zoom
        movement += speed * Time.deltaTime;
        movement *= (1 - (Time.deltaTime * zoom_speed));
        cam.orthographicSize = Mathf.Lerp(min_size, max_size, movement);

        transform.position = Vector3.MoveTowards(transform.position, dest, Time.deltaTime * speed);

    }


    void OnDrawGizmosSelected() {
        Vector3 top_left = new Vector3(boundaries.xMin, boundaries.yMin, transform.position.z);
        Vector3 top_right = new Vector3(boundaries.xMax, boundaries.yMin, transform.position.z);
        Vector3 bottom_left = new Vector3(boundaries.xMin, boundaries.yMax, transform.position.z);
        Vector3 bottom_right = new Vector3(boundaries.xMax, boundaries.yMax, transform.position.z);

        Gizmos.color = Color.red;
        Gizmos.DrawLine(top_left, top_right);
        Gizmos.DrawLine(top_right, bottom_right);
        Gizmos.DrawLine(bottom_right, bottom_left);
        Gizmos.DrawLine(bottom_left, top_left);

        Gizmos.color = Color.green;
        Gizmos.DrawIcon(transform.position + new Vector3(offset.x, offset.y, 0), "eye.png");

    }
}
