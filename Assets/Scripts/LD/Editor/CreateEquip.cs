﻿using UnityEditor;
using UnityEngine;

namespace LD
{
    public class CreateEquip
    {
        [MenuItem("Assets/Create/Equip")]
        public static Equip Create()
        {
            Equip asset = ScriptableObject.CreateInstance<Equip>();

            AssetDatabase.CreateAsset(asset, "Assets/Data/Equipment/Equip.asset");
            AssetDatabase.SaveAssets();
            return asset;
        }
    }
}