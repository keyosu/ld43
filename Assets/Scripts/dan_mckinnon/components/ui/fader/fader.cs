﻿// Dan's Jam Pack for Unity #1.
// ---
// by Dan Player McKinnon
// E-mail: dan@danmckinnon.net
// Created: February 2016
// Description:
//		This asset pack gives some miscellanous boilerplate functionality
//		that are very handy when prototyping and jamming out games.
// Liscence:  
//		This work is licensed under the Creative Commons Attribution 4.0 International License. 
//		To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/.

// Fader
// ---
// A UI element that fades to black, white or any other colour.
// although primarily designed for full-screen fades this component
// works on any UI image or UI text element.
//Great for making flashes too

using UnityEngine;
using System.Collections;
using UnityEngine.Events;
using UnityEngine.UI;

public enum fader_state { stopped,fade_in,fade_out}

[AddComponentMenu("Dan McKinnon/UI/Fader")]
public class fader : MonoBehaviour {

    [Tooltip("How long the transition takes.")]
    public float duration = 1.0f;
    [Tooltip("The alpha animation curve")]
    public AnimationCurve curve;
    private float start_time;
    private fader_state state;
    private UnityEvent on_over = null;
    private Image image;
    private Color start_colour;
    private Color end_colour;
    private float end_time;
    private Text text;
    void Start () {
        enabled = false;
        image = GetComponent<Image>();
        text = GetComponent<Text>();
        if (image) {
            start_colour = new Color(image.color.r, image.color.g, image.color.b, 1);
            end_colour = new Color(image.color.r, image.color.g, image.color.b, 0);
        } else if (text) {
            start_colour = new Color(text.color.r, text.color.g, text.color.b, 1);
            end_colour = new Color(text.color.r, text.color.g, text.color.b, 0);
        } else { 
            Debug.LogWarning("Objects with fader component must have a UI image or UI text component attached as well.");
        }
	}
    // fade_in()
    // ---
    // Begins the fade in transition.  E.x. black to screen.
    public void fade_in() {
        start_fade(fader_state.fade_in, null);
    }
    // fade_out()
    // ---
    // Begins the fade out transition.  E.x. screen to black.
    public void fade_out() {
        start_fade(fader_state.fade_out, null);
    }

    // fade_in(event)
    // ---
    //	event - an event that is triggered when the transition is over
    // Begins the fade in transition.  E.x. black to screen.
    public void fade_in(UnityEvent on_fade_in) {
        start_fade(fader_state.fade_in, on_fade_in);
    }
    // fade_out(event)
    // ---
    //	event - an event that is triggered when the transition is over
    // Begins the fade out transition.  E.x. screen to black.
    public void fade_out(UnityEvent on_fade_in) {
        start_fade(fader_state.fade_out, on_fade_in);
    }

    // set_duration( n )
    // ---
    //	n - length in seconds
    // Set the length of the transition.
    public void set_duration( float n) {
        duration = n;
    }

    private void start_fade( fader_state state, UnityEvent on_over) {
        start_time = Time.time;
        end_time = start_time + duration;
        this.state = state;
        this.on_over = on_over;
        enabled = true;

    }



    // fade_in( event, duration)
    // ---
    //	event - to be triggered when the transition is over
    //	duration - length in seconds
    // Begins the fade in transition.  E.x. black to screen.
    public void fade_in(UnityEvent on_fade_in, float duration) {
        set_duration(duration);
        fade_in(on_fade_in);
    }
    // fade_out( event, duration)
    // ---
    //	event - to be triggered when the transition is over
    //	duration - length in seconds
    // Begins the fade out transition.  E.x. screen to black.
    
    public void fade_out(UnityEvent on_fade_in, float duration) {
        set_duration(duration);
        fade_out(on_fade_in);
    }


    void Update() {



        float t = Mathf.Clamp((Time.time - start_time) / duration, 0, 1);
        float alpha = curve.Evaluate(state == fader_state.fade_in ? t : 1 - t);
        if ( image) {

            image.color = Color.Lerp(start_colour, end_colour, alpha);
        } else if ( text) {

            text.color = Color.Lerp(start_colour, end_colour, alpha);

        }

        if ( Time.time >= end_time) {
            if (on_over != null) {
                on_over.Invoke();
            }
            enabled = false;
        }

    }
}
