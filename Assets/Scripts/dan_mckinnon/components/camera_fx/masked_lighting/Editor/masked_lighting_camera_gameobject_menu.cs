﻿// Dan's Jam Pack for Unity #1.
// ---
// by Dan Player McKinnon
// E-mail: dan@danmckinnon.net
// Created: February 2016
// Description:
//		This asset pack gives some miscellanous boilerplate functionality
//		that are very handy when prototyping and jamming out games.
// Liscence:  
//		This work is licensed under the Creative Commons Attribution 4.0 International License. 
//		To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/.

// Masked Lighting Menu
// ---
// Conveniently adds masked lighting to the scene.
// Does the entire workflow and prompts the user to make sure they are get the desired
// outcome every time with little knowledge about how the masked lighting works.
using UnityEngine;
using System.Collections;
using UnityEditor;
using System.IO;

public class masked_lighting_camera_gameobject_menu {
    public static void create_lighting_layer() {
        SerializedObject tag_manager = new SerializedObject(AssetDatabase.LoadAllAssetsAtPath("ProjectSettings/TagManager.asset")[0]);
        SerializedProperty layer_properties = tag_manager.FindProperty("layers");

        SerializedProperty sp;
        bool done = false;
        for (int i = 8; !done && i < layer_properties.arraySize; i++) {
            sp = layer_properties.GetArrayElementAtIndex(i);
            if ( sp.stringValue == "") {
                sp.stringValue = "Masked Lighting";
                done = true;
            }
         }
        
        tag_manager.ApplyModifiedProperties();

    }

	//
    // ---
    //
    //
    [MenuItem("GameObject/Dan McKinnon/Camera FX/Masked lighting camera", false, 10)]
    public static void masked_lighting_camera() {

        Camera cam;
        // Add Masked Lighting Layer if applicable
        LayerMask mask = LayerMask.GetMask("Masked Lighting");
        if (mask.value == 0) {
            bool create_layer = EditorUtility.DisplayDialog("Create layer?", "A masked lighting layer has not been found.  Would you like to create one?", "Yes", "No");
            if (create_layer) {
                create_lighting_layer();
            }
            mask = LayerMask.GetMask("Masked Lighting");

        }

        // Attach to main camera if applicable
        GameObject o = null;
        string name = "masked_lighting_camera";
        if (Camera.main) {
            bool answer = EditorUtility.DisplayDialog("Attach to main camera", "Attach masked lighting component to main camera instead?", "Yes", "No");
            if (answer) {

                o = Camera.main.gameObject;
                if (!Camera.main.orthographic) {
                    bool answer2 = EditorUtility.DisplayDialog("Change perspective", "This component requires a 2D orthographic projection to work correctly.  Change perspective now?", "Yes", "No");
                    if (answer2) {
                        Camera.main.orthographic = true;
                        Camera.main.orthographicSize = 4;
                    }
                }

                if (o.GetComponent<masked_lighting_filter>()) {
                    EditorUtility.DisplayDialog("Masked lighting filter already attached", "A masked lighting filter component was already attached to the main camera.", "Ok");
                    return;
                } else {

                    Undo.AddComponent<masked_lighting_filter>(o);
                }
            }
        }

        //Create object if applicable
        if (o == null) {
            o = new GameObject(name);
            if (!Camera.main) {
                o.tag = "MainCamera";
            }

            cam = o.AddComponent<Camera>();
            cam.orthographic = true;
            cam.orthographicSize = 4;

            o.AddComponent<AudioListener>();
            o.AddComponent<GUILayer>();
            o.AddComponent<masked_lighting_filter>();


            Undo.RegisterCreatedObjectUndo(o, "create masked lighting camera");
        }


        //Attach rendering materials
        masked_lighting_filter filter = o.GetComponent<masked_lighting_filter>();
        filter.render_material = AssetDatabase.LoadAssetAtPath<Material>("Assets/dan_mckinnon/components/camera_fx/masked_lighting/masked_lighting_default_render_material.mat");
        filter.lighting_render_material = AssetDatabase.LoadAssetAtPath<Material>("Assets/dan_mckinnon/components/camera_fx/masked_lighting/masked_lighting_default_lighting_material.mat");
        bool create_new_material = EditorUtility.DisplayDialog("Create new material asset", "Create new lighting material asset?", "Yes", "No");
        if (create_new_material) {
            string path = AssetDatabase.GenerateUniqueAssetPath("Assets/new_masked_lighting_material.mat");
            AssetDatabase.CopyAsset("Assets/dan_mckinnon/components/camera_fx/masked_lighting/masked_lighting_default_lighting_material.mat", path);
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();

            filter.lighting_render_material = AssetDatabase.LoadAssetAtPath<Material>(path);
        } else {
            filter.lighting_render_material = AssetDatabase.LoadAssetAtPath<Material>("Assets/dan_mckinnon/components/camera_fx/masked_lighting/masked_lighting_default_lighting_material.mat");
        }

        cam = o.GetComponent<Camera>();

        //Create light camera
        bool add_light_camera = EditorUtility.DisplayDialog("Add light camera", "This component requires a second camera to render the lighting layer.  Would you like to add this object to the scene now?", "Yes", "No");
        if (add_light_camera) {

            var light_camera = new GameObject("light_rendering_camera");
            Camera light_camera_camera = light_camera.AddComponent<Camera>();
            light_camera_camera.orthographic = cam.orthographic;
            light_camera_camera.orthographicSize = cam.orthographicSize;
            light_camera.transform.position = o.transform.position;
            light_camera.transform.localScale = o.transform.localScale;

            light_camera.transform.SetParent(o.transform);
            light_camera_camera.backgroundColor = Color.clear;
            light_camera_camera.cullingMask = LayerMask.GetMask("Masked Lighting");
            cam.cullingMask = cam.cullingMask & ~LayerMask.GetMask("Masked Lighting");

            masked_lighting_filter f = o.GetComponent<masked_lighting_filter>();
            if ( f) {
                f.light_camera = light_camera_camera;
            }
            Undo.RegisterCreatedObjectUndo(light_camera,"create light camera");
        } else {
            EditorUtility.DisplayDialog("Warning", "A lighting camera could not be found.  You will need to manually set a camera to render lighting into your scene", "Ok");

        }

    }

    [MenuItem("Assets/Create/Dan McKinnon/Masked lighting material", false, 10)]
    public static void add_masked_lighting_material() {
        string path = AssetDatabase.GetAssetPath(Selection.activeObject);
        if (path == "") {
            path = "Assets";
        } else if (Path.GetExtension(path) != "") {
            path = path.Replace(Path.GetFileName(AssetDatabase.GetAssetPath(Selection.activeObject)), "");
        }

        path = AssetDatabase.GenerateUniqueAssetPath(path + "/new_masked_lighting_material.mat");
        AssetDatabase.CopyAsset("Assets/dan_mckinnon/components/camera_fx/masked_lighting/masked_lighting_default_lighting_material.mat", path);
        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();
    }


}
