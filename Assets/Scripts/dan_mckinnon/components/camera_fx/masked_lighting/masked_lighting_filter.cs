﻿// Dan's Jam Pack for Unity #1.
// ---
// by Dan Player McKinnon
// E-mail: dan@danmckinnon.net
// Created: February 2016
// Description:
//		This asset pack gives some miscellanous boilerplate functionality
//		that are very handy when prototyping and jamming out games.
// Liscence:  
//		This work is licensed under the Creative Commons Attribution 4.0 International License. 
//		To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/.

// Masked lighting filter
// ---
// This component is meant to be used in conjunction with a masked lighting material
// and a 'lighting camera'.
// You must set up a secondary camera that only renders lighting sprites
// Lighting sprites will then be used to "cut out" the underlying colour
// from a completely black (or coloured) overlay.
// A little bit of the light sprite will then be blended in to give the light its colour.
//
// Note that light sprites are normal sprites and any shape or bitmap will work,
// but to get a more natural effect use a white to alpha gradient and adjust the colour
// in the sprites colour property.
using UnityEngine;
using UnityEngine.Rendering;
using System.Collections;

[AddComponentMenu("Dan McKinnon/Camera FX/Masked lighting filter")]
public class masked_lighting_filter : MonoBehaviour {

	// Use this for initialization
	void Start () {

        
    }

    private RenderTexture light_layer;
    [Tooltip("A camera that renders light sprites.")]
    public Camera light_camera;
    [Tooltip("Generally you will use a normal sprite rendering material.")]
    public Material render_material;
    [Tooltip("Generally you will use a masked light rendering material.  Go to assets->Create->Dan McKinnon->Masked lighting material to create one.")]
    public Material lighting_render_material;


	// Update is called once per frame
	void Update () {
	
	}

    
    void update_lighting_layer_size( int width, int height) {
        if ( light_layer == null || light_layer.width != width || light_layer.height != height) {           
            light_layer = new RenderTexture(width, height, 24);
            light_camera.targetTexture = light_layer;
            lighting_render_material.SetTexture("_lighting", light_layer);
        }

    }


    void OnRenderImage(RenderTexture source, RenderTexture destination) {
        if (source != null) {
            update_lighting_layer_size(source.width, source.height);
        }

        Camera cam = GetComponent<Camera>();
        light_camera.orthographicSize = cam.orthographicSize;
        light_camera.transform.position = cam.transform.position;
        light_camera.transform.localScale = cam.transform.localScale;
        Graphics.Blit(source, destination, render_material);
        Graphics.Blit(source, destination, lighting_render_material);
    }
}
