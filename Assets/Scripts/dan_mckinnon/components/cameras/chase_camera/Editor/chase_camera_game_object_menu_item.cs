﻿// Dan's Jam Pack for Unity #1.
// ---
// by Dan Player McKinnon
// E-mail: dan@danmckinnon.net
// Created: February 2016
// Description:
//		This asset pack gives some miscellanous boilerplate functionality
//		that are very handy when prototyping and jamming out games.
// Liscence:  
//		This work is licensed under the Creative Commons Attribution 4.0 International License. 
//		To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/.

// Chase camera menu item
// ---
// Conveniently create a chase camera
//
using UnityEngine;
using System.Collections;
using UnityEditor;


public class chase_camera_game_object_menu_item {

    //
    // ---
    //
    //
    [MenuItem("GameObject/Dan McKinnon/Cameras/Chase camera 2D",false,10)]
    public static void add_game_object() {
        //GameObject clone = (GameObject)PrefabUtility.InstantiatePrefab((GameObject)AssetDatabase.LoadAssetAtPath("Assets/dan_mckinnon/game_objects/cameras/chase_camera/chase_camera.prefab", typeof(GameObject)));
        //clone.transform.SetParent(Selection.activeGameObject.transform);
        //Undo.RegisterCreatedObjectUndo(clone,"create chase camera");

        // -- Ask if the user would like to replace the main camera if one has been found -- //
        GameObject o;
        string name = "chase_camera";
        if (Camera.main) {
            bool answer = EditorUtility.DisplayDialog("Add chase camera", "Attach chase component to main camera instead?", "Yes", "No" );
            if (answer) {

                o = Camera.main.gameObject;
                if (!Camera.main.orthographic) {
                    bool answer2 = EditorUtility.DisplayDialog("Change perspective", "This component requires a 2D orthographic projection to work correctly.  Change perspective now?", "Yes", "No");
                    if (answer2) {
                        Camera.main.orthographic = true;
                        Camera.main.orthographicSize = 4;
                    }
                }

                if (o.GetComponent<chase_camera>()) {
                    EditorUtility.DisplayDialog("Chase camera already attached", "A chase camera component was already attached to the main camera.", "Ok");
                } else {

                    Undo.AddComponent<chase_camera>(o);
                }
                return;
            }
        }

        o = new GameObject(name);
        if (!Camera.main) {
            o.tag = "MainCamera";
        }

        o.AddComponent<Camera>();
        Camera cam = o.GetComponent<Camera>();
        cam.orthographic = true;
        cam.orthographicSize = 4;

        o.AddComponent<AudioListener>();
        o.AddComponent<GUILayer>();
        o.AddComponent<chase_camera>();

        Undo.RegisterCreatedObjectUndo(o,"create chase camera");


    }




}
