﻿using TMPro;
using UnityEngine.EventSystems;

namespace LD
{
    public class CommandUI : ButtonUI
    {
        public Skill skill;
        public TextMeshProUGUI Alt;

        public override void OnPointerEnter(PointerEventData eventData)
        {
            BattleManager.instance.UpdateActionInfo(skill.ParsedDescription);
        }

        public override void OnPointerExit(PointerEventData eventData)
        {
            BattleManager.instance.UpdateActionInfo(string.Empty);
        }

        public override void Setup()
        {
        }

        public void PerformSkill()
        {
            if (BattleManager.instance.CurrentEntity.CurrentMP > skill.Cost * 3)
            {
                BattleManager.instance.PlayerPerformSkill(skill);
            }
        }
    }
}

