﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Dan McKinnon/Camera FX/CRT Digital Distortion" {
	Properties{
		_scanline_displacement("Scanline displacement", Range(0,5)) = 1.0
		_noise_level("Noise level", Range(0,5)) = 1.0
		_colour_bias("Colour bias", Range(0,1)) = 0.5
		_colour_noise("Colour noise",Range(0,1)) = 0.1
		_image_warping("Image warping",Range(-5,5)) = 1.0
		_MainTex("", 2D) = "white" {}
	}

		SubShader{

			ZTest Always Cull Off ZWrite Off Fog{ Mode Off } //Rendering settings

			Pass{
				CGPROGRAM
#pragma vertex vert
#pragma fragment frag
#include "UnityCG.cginc" 
		

			struct v2f {
				float4 position : POSITION;
				half2 uv : TEXCOORD0;
			};

			v2f vert(appdata_img v) {
				v2f o;
				o.position = UnityObjectToClipPos(v.vertex);
				o.uv = MultiplyUV(UNITY_MATRIX_TEXTURE0, v.texcoord.xy);
				return o;
			}

			float _scanline_displacement;
			float _noise_level;
			float _colour_bias;
			float _colour_noise;
			float _image_warping;
			sampler2D _MainTex;

			float Random(float2 in_position){
				float2 input = in_position;
				const float2 random_vector = float2(15.12345, 91.98765);
				float random_angle = dot(input, random_vector);
				float random_sample = sin(random_angle) * 115309.76543;
				return 1.0 - 2.0 * frac(random_sample);
			}

			fixed4 frag(v2f i) : COLOR{
				float2 d = i.uv;
				if (_image_warping > 0) {
					
					d = float2(pow(i.uv.x , _image_warping) , i.uv.y);
				}
				float t = _Time.y;
				float random_sample1 = Random(i.uv.y + t);
				float random_sample2 = Random(i.uv + t);
				float modified_tone = _colour_bias + (random_sample2 * _colour_noise);
				float random_scan_sample = random_sample1 * 0.01f * _scanline_displacement;
				float random_noise_sample = random_sample2 * _noise_level;
				fixed4 original_colour = tex2D(_MainTex, d + float2(random_scan_sample,0)  );
											   
				float average = (original_colour.r + original_colour.g + original_colour.b + random_noise_sample) / 4.0f;
				fixed4 output_colour = fixed4(average + original_colour.r * modified_tone, average + original_colour.g * modified_tone, average + original_colour.b * modified_tone, 1);

				return output_colour;
			}
			ENDCG
		}
	}
		FallBack "Diffuse"
}