﻿// Dan's Jam Pack for Unity #1.
// ---
// by Dan Player McKinnon
// E-mail: dan@danmckinnon.net
// Created: February 2016
// Description:
//		This asset pack gives some miscellanous boilerplate functionality
//		that are very handy when prototyping and jamming out games.
// Liscence:  
//		This work is licensed under the Creative Commons Attribution 4.0 International License. 
//		To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/.

// Iniital velocity editor
// ---
// Visually edit an object's initial velocity.
//
using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(initial_velocity))]
[CanEditMultipleObjects()]
public class initial_velocity_editor : Editor {

    void OnSceneGUI() {
        initial_velocity t = (initial_velocity)target;

        Undo.RecordObject( (Object)target, "change initial velocity");
        EditorGUI.BeginChangeCheck();
        Vector3 pos = Handles.PositionHandle(t.transform.position + t.velocity, Quaternion.identity);
        if (EditorGUI.EndChangeCheck()) {
            t.velocity = pos - t.transform.position;
        }

    }

}
