﻿// Dan's Jam Pack for Unity #1.
// ---
// by Dan Player McKinnon
// E-mail: dan@danmckinnon.net
// Created: February 2016
// Description:
//		This asset pack gives some miscellanous boilerplate functionality
//		that are very handy when prototyping and jamming out games.
// Liscence:  
//		This work is licensed under the Creative Commons Attribution 4.0 International License. 
//		To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/.

// Modular song
// ---
// For use with the modular song player component.  
// To use this,  cut your song into parts (e.x. "Intro, Verse, Chorus)
// Create several children with Audio Sources attached
// The player will play each child in succession,  repeating at a given point.

using UnityEngine;
using System.Collections;

[AddComponentMenu("Dan McKinnon/Music/Modular music/Modular song")]
public class module_song : MonoBehaviour {
	[Tooltip("Ignore the first number of game objects after repeating once.  Set to 0 to repeat at the start of the song")]
    public int repeat_position = 0;

}
