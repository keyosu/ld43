﻿using UnityEngine;

namespace LD
{
    [System.Serializable]
    public class Item : ScriptableObject
    {
        public string Name;
        public string Description;

        public int Recovery;
        public bool HealAilments;
        public bool Resurrect;
    }
}
