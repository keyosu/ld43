﻿using DG.Tweening;
using MEC;
using Sirenix.OdinInspector;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace LD
{
    public class MainManager : MonoBehaviour
    {
        public enum Interruption
        {
            Inn, Encounter, Fortune, Shop, Skills
        }

        public static MainManager instance;
        public bool IsPlaying = true;
        public bool InAction = false;
        public float DifficultyModifier = 2f;

        private int coins;
        public int Coins
        {
            get
            {
                CoinsText.text = coins.ToString("00");
                return coins;
            }
            set
            {
                coins = value;
                CoinsText.text = coins.ToString("00");
            }
        }
        public TextMeshProUGUI CoinsText;

        [Space(5)]
        public Canvas InnCanvas;
        public CanvasGroup InnCanvasGroup;
        public AudioClip LevelMusic;
        public AudioClip BattleMusic;

        [Space(5)]
        public Canvas SkillCanvas;
        public CanvasGroup SkillCanvasGroup;
        public TextMeshProUGUI SkillQuestion;
        public List<TextMeshProUGUI> ShownSkills = new List<TextMeshProUGUI>();

        [Space(5)]
        public float TraveledDistance = 0f;
        public float TravelSpeed = 1.5f;
        public float TravelEnding = 100f;
        public TextMeshProUGUI TravelProgressText;
        public Image TravelProgressImage;

        [Space(5)]
        public Transform MoveTransform;
        public GameObject LightCamera;

        [Space(10)]
        public List<Entity> Allies = new List<Entity>();

        [Space(10)]
        public List<Transform> PossibleSpawnLocations = new List<Transform>();

        [Space(10)]
        public List<Entity> PossibleEnemies = new List<Entity>();

        [Space(10)]
        public List<Skill> AllSkills = new List<Skill>();

        private int encounterCount = 1;
        private int ally = 1;
        private List<Skill> availableSkills = new List<Skill>();
        private Queue<Interruption> TravelInterruptions = new Queue<Interruption>();
        private Queue<float> TravelIntervals = new Queue<float>();

        [Button]
        public void CalcSkillRatings()
        {
            GetAllSkills();
            foreach (Skill skill in AllSkills)
            {
                skill.OnValidate();
                skill.CalculateRating();
            }
        }

        [Button]
        public void GetAllSkills()
        {
            AllSkills = new List<Skill>(Resources.FindObjectsOfTypeAll<Skill>());
        }

        public void EndAction()
        {
            InAction = false;
        }

        #region --- Unity Methods ---

        private void Awake()
        {
            instance = this;
            LightCamera.SetActive(true);
            PopulateTravel();
        }

        private void Start()
        {
            for (int i = 0; i < Allies.Count; i++)
            {
                Allies[i].Init();
            }
        }

        private void Update()
        {
            if (IsPlaying && !InAction)
            {
                for (int i = 0; i < Allies.Count; i++)
                {
                    Allies[i].m_Animator.SetBool("Walk", true);
                }
                Timing.RunCoroutine(_Travel());
            }
        }

        #endregion

        public void RestAtInn()
        {

        }

        public void Cancel()
        {
            InnCanvas.gameObject.SetActive(false);
            SkillCanvas.gameObject.SetActive(false);
            Invoke("EndAction", 0.5f);
        }

        public void GetSkill(int skill)
        {
            Allies[ally - 1].skills.Add(availableSkills[skill]);
            if (ally == 1)
            {
                ally = 2;
                RegenerateSkills();
            }
            else
            {
                Cancel();
            }
        }

        private void PopulateTravel()
        {
            TravelInterruptions.Clear();
            int interruptionCount = Random.Range(40, 80);
            TravelInterruptions.Enqueue(Interruption.Skills);
            TravelInterruptions.Enqueue(Interruption.Encounter);

            for (int i = 0; i < interruptionCount; i++)
            {
                Interruption interruption;
                float weight = Random.Range(0, 100);
                if (weight < 10)
                {
                    interruption = Interruption.Inn;
                }
                else if (weight < 30)
                {
                    interruption = Interruption.Skills;
                }
                else if (weight < 50)
                {
                    interruption = Interruption.Shop;
                }
                else
                {
                    interruption = Interruption.Encounter;
                }
                TravelIntervals.Enqueue(TravelEnding / interruptionCount);
                TravelInterruptions.Enqueue(interruption);
            }
        }

        private void DoAction(Interruption interruption)
        {
            for(int i = 0; i < Allies.Count; i++)
            {
                Allies[i].m_Animator.SetBool("Walk", false);
            }
            switch (interruption)
            {
                case Interruption.Fortune:
                    Debug.Log("Fortune");
                    Cancel();
                    break;
                case Interruption.Shop:
                    Debug.Log("Shop");
                    Coins += Random.Range(1, 50);
                    Cancel();
                    break;
                case Interruption.Inn:
                    InnCanvasGroup.alpha = 0;
                    InnCanvasGroup.DOFade(1f, 0.25f);
                    InnCanvas.gameObject.SetActive(true);
                    break;
                case Interruption.Encounter:
                    Timing.RunCoroutine(_Encounter());
                    break;
                case Interruption.Skills:
                    ally = 1;
                    RegenerateSkills();
                    SkillCanvasGroup.alpha = 0;
                    SkillCanvasGroup.DOFade(1f, 0.25f);
                    SkillCanvas.gameObject.SetActive(true);
                    break;
            }
        }

        private void RegenerateSkills()
        {
            SkillQuestion.text = "Pick a skill for ally " + ally.ToString();

            int loop = 15;
            availableSkills.Clear();
            while (availableSkills.Count < 6 && loop > 0)
            {
                Skill randomSkill = AllSkills[Random.Range(0, AllSkills.Count)];
                if (!availableSkills.Contains(randomSkill) && !Allies[ally - 1].skills.Contains(randomSkill))
                {
                    availableSkills.Add(randomSkill);
                }
                else
                {
                    loop--;
                }
            }
            if (loop == 0)
            {
                for (int i = availableSkills.Count - 1; i < ShownSkills.Count; i++)
                {
                    ShownSkills[i].text = string.Empty;
                }
            }
            for (int i = 0; i < availableSkills.Count; i++)
            {
                ShownSkills[i].text = availableSkills[i].Name + "\n\n<size=14>" + availableSkills[i].ParsedDescription;
            }
        }

        private IEnumerator<float> _Travel()
        {
            InAction = true;
            float travelAmount = Mathf.Round(TravelIntervals.Dequeue() * 100f) / 100f;
            float travelTime = travelAmount / TravelSpeed * Random.Range(2.5f, 3.5f);
            MoveTransform.DOMove(MoveTransform.position + Vector3.right * travelAmount, travelTime);
            TraveledDistance += travelAmount;
            yield return Timing.WaitForSeconds(travelTime);

            TravelProgressText.text = TraveledDistance.ToString() + "%";
            TravelProgressImage.fillAmount = TraveledDistance / TravelEnding;
            DoAction(TravelInterruptions.Dequeue());
        }

        private IEnumerator<float> _Encounter()
        {
            float difficulty = Mathf.RoundToInt(encounterCount * DifficultyModifier);
            int enemyCount = Random.Range(1, 4);
            int spawnCount = 0;

            float enemyPower = difficulty / enemyCount;

            while (enemyCount > 0)
            {
                Entity enemy = null;
                int enemyLoop = 10;
                while (enemy == null && enemyLoop > 0)
                {
                    Entity chosenEnemy = PossibleEnemies[Random.Range(0, PossibleEnemies.Count)];
                    if (!chosenEnemy.gameObject.activeSelf)
                    {
                        enemy = chosenEnemy;
                    }
                    enemyLoop--;
                }
                if (enemy != null)
                {
                    EntitySetup(enemy, enemyPower, PossibleSpawnLocations[spawnCount]);
                    spawnCount++;
                }
                enemyCount--;
            }
            encounterCount++;

            PlayMusic.instance.PlaySelectedMusic(BattleMusic);
            yield return Timing.WaitForSeconds(0.5f);
            BattleManager.instance.InitiateBattle();
        }

        private void EntitySetup(Entity entity, float power, Transform spawnLoc)
        {
            entity.LVL = Mathf.CeilToInt(power / 2f);
            entity.PopulateStats();
            entity.Init();
            entity.PopulateSkills(power);
            entity.EndPosition = spawnLoc.position;

            entity.gameObject.SetActive(true);
            entity.Enter();
        }
    }
}