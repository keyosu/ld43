﻿using UnityEngine.EventSystems;

namespace LD
{
    public class ItemUI : ButtonUI
    {
        public Item item;

        public override void Setup()
        {
            Title.text = item.Name;
        }
        public override void OnPointerEnter(PointerEventData eventData)
        {

        }
        public override void OnPointerExit(PointerEventData eventData)
        {
        }
    }
}
