﻿// Dan's Jam Pack for Unity #1.
// ---
// by Dan Player McKinnon
// E-mail: dan@danmckinnon.net
// Created: February 2016
// Description:
//		This asset pack gives some miscellanous boilerplate functionality
//		that are very handy when prototyping and jamming out games.
// Liscence:  
//		This work is licensed under the Creative Commons Attribution 4.0 International License. 
//		To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/.

// Fly-in menu
// ---
// A convenience menu animation.   Menus 'fly in' and 'fly-out' according to curves
// when the menu is paged.
// 
// Direct descendants of a fly-in menu are treated as 'pages'
// Children of pages will fly-in and fly-out when the menu system is paged.
//
using UnityEngine;
using System.Collections;
using UnityEngine.Events;

public enum delay_direction { forward, backward }
public enum fly_in_menu_state { closed, open, entering, exiting };

[System.Serializable]
public class fly_transition_class {
    [Tooltip("How fast menu items fly.")]
    public float speed = 0.5f;
    [Tooltip("The delay between fly-in/fly-out queues")]
    public float delay = 0.25f;
    [Tooltip("Forward means delay fly-in/fly-out from top to bottom in heirarchy.  Backward means delay fly-in from bottom to top in heirarchy.")]
    public delay_direction direction = delay_direction.forward;

    [Tooltip("The curve the menu items follow.  A value of 1 represents the natural position. ")]
    public AnimationCurve x_curve = new AnimationCurve(new Keyframe[2] {new Keyframe(0,1),new Keyframe(1,0) });
    [Tooltip("The curve the menu items follow.  A value of 1 represents the natural position. ")]
    public AnimationCurve y_curve = new AnimationCurve(new Keyframe[2] { new Keyframe(0, 0), new Keyframe(1, 0) });
    [Tooltip("X scaling of the transition curve")]
    public AnimationCurve sx_curve = new AnimationCurve(new Keyframe[2] { new Keyframe(0, 1), new Keyframe(1, 1) });
    [Tooltip("Y Scaling of the transition curve")]
    public AnimationCurve sy_curve = new AnimationCurve(new Keyframe[2] { new Keyframe(0, 1), new Keyframe(1, 1) });
    [Tooltip("Rotation of the menu items")]
    public AnimationCurve rotation_curve = new AnimationCurve(new Keyframe[2] { new Keyframe(0, 0), new Keyframe(1, 0) });

    [Tooltip("Triggered when the transition starts")]
    public UnityEvent on_begin;
    [Tooltip("Triggered when the transition is over")]
    public UnityEvent on_done;
    [Tooltip("Triggered every time an object is queued to fly-in/fly-out")]
    public UnityEvent on_fly_object;
    public fly_transition_class() {

    }
    // 
    // ---
    //    
    public fly_transition_class(AnimationCurve x, AnimationCurve y) {
        x_curve = x;
        y_curve = y;
    }
}

public class initial_fly_in_values_class {
    public GameObject o;
    public float x;
    public float y;
    public float sx;
    public float sy;
    public float rotation;
    public float start_time;
    public bool fly_invoked;
    public bool is_exception;
}


[AddComponentMenu("Dan McKinnon/UI/Fly-in menu")]
public class fly_in_menu : MonoBehaviour {

    // -- PUBLIC INSPECTOR VALUES -- //
    [Tooltip("Children on this list will not transition.  Set fly-in exception component on these items to add extra functionality when the item would normally fly-in/fly-out.")]
    public GameObject[] exceptions;
    [Tooltip("Entry fly-in transition")]
    public fly_transition_class entry;
    [Tooltip("Exit fly-out transition")]
    public fly_transition_class exit =new fly_transition_class(new AnimationCurve(new Keyframe[2] { new Keyframe(0, 0), new Keyframe(1, 0) }), new AnimationCurve(new Keyframe[2] { new Keyframe(0, 0), new Keyframe(1, -1) }));
    [Tooltip("Set this to a direct child of the fly-in menu.")]
    public GameObject default_menu;



    // -- PRIVATE -- //
    private fly_in_menu_state state = fly_in_menu_state.closed;
    private float end_time;
    private float start_time;
    private fly_transition_class transition;
    private GameObject page;
    private GameObject next_page;
    private ArrayList catalog = null;


    private void do_transition( fly_transition_class t, fly_in_menu_state next_state ) {

        if (page != null) {
            //Debug.Log("Transition begin");
            transition = t;

            start_time = Time.time;
            float duration = (page.transform.childCount * t.delay) + t.speed;
            end_time = start_time + duration;
            state = next_state;
            page.SetActive(true);
            catalog_game_object(page);
            set_starting_positions();
            enabled = true;

            t.on_begin.Invoke();
        } else {
            Debug.LogWarning("do_transition() on fly_in_menu with no page selected");
        }

    }


    private void set_starting_positions() {
        foreach (initial_fly_in_values_class initial_values in catalog) {
            RectTransform rect = initial_values.o.GetComponent<RectTransform>();
            if (initial_values.is_exception) {
                rect.position = new Vector3(initial_values.x, initial_values.y, 0);
            } else { 
               rect.position = new Vector3(initial_values.x + (transition.x_curve.Evaluate(0) * Screen.width), initial_values.y + (transition.y_curve.Evaluate(0) * Screen.height), 0);
            }
        }
    }

    private bool close_was_called = false;
    private bool open_was_called = false;    

    // close()
    // ---
    // Close the current menu, page  triggering the appropriate fly-out transition
    // Will wait for the current transition to be over before triggering.
    public void close() {
        switch( state) {
            case fly_in_menu_state.entering:
                close_was_called = true;
                break;
            case fly_in_menu_state.exiting:
                close_was_called = true;
                break;

            case fly_in_menu_state.open:
                open_was_called = false;
                close_was_called = false;
                do_transition(exit, fly_in_menu_state.exiting);
            break;
        }
    }
    

    // open( page )
    // ---
    //	page - should be a direct child of the fly-in menu
    // Opens a menu page, triggering the appropriate fly-in animation.
    // Will wait for the current transition to be over before triggering.
    public void open( GameObject new_page ) {
        
        switch ( state) {
            case fly_in_menu_state.open:
                //Debug.Log("Opening while open");
                close();
                open_was_called = true;
                next_page = new_page;

                break;

            case fly_in_menu_state.closed:
                //Debug.Log("Open while closed");
                open_was_called = false;
                close_was_called = false;
                page = new_page;
                
                do_transition(entry, fly_in_menu_state.entering);

                break;
            case fly_in_menu_state.entering:
                //Debug.Log("Opening while entering");
                open_was_called = true;
                next_page = new_page;
                break;
            case fly_in_menu_state.exiting:
                //Debug.Log("Opening while exiting");
                open_was_called = true;
                next_page = new_page;
                break;
        }
    }






    //re-catalog every menu item's default transformation, rotation and scale values
    private void catalog_game_object( GameObject parent ) {

        if ( catalog != null ) {
            catalog.Clear();
        }
        catalog = new ArrayList();


        float num = parent.transform.childCount;

        int n = 0;


        //count number of children that are not exceptions.
        foreach (Transform child in parent.transform) {
            foreach (GameObject obj in exceptions) {
                if ( obj == child.gameObject) {
                    num--;
                }
            }
        }

        //for each object in page
        foreach ( Transform child in parent.transform) {

            
            initial_fly_in_values_class initial_values = new initial_fly_in_values_class();
            foreach ( GameObject obj in exceptions) {
                if ( obj == child.gameObject) {
                    initial_values.is_exception = true;
                }
            }
            
            if (!initial_values.is_exception) {
                n++;
            }
            GameObject o = child.gameObject;
            initial_values.o = o;
            RectTransform rect = o.GetComponent<RectTransform>();
            initial_values.x = rect.position.x;
            initial_values.y = rect.position.y;
            initial_values.sx = rect.localScale.x;
            initial_values.sy = rect.localScale.y;
            initial_values.rotation = rect.rotation.z;
            initial_values.fly_invoked = false;

            if (transition.direction == delay_direction.forward) {

                initial_values.start_time = start_time + (n * transition.delay);
            } else {
                initial_values.start_time = start_time + ((num - n) * transition.delay);
            }

            catalog.Add(initial_values);
        }
    }



    void Start () {
        //go through every child and disable it
        foreach ( Transform child in transform) {
            child.gameObject.SetActive(false);
        }
        enabled = false;

        
        //open a default menu if one is specified
        if ( default_menu != null) {
            open(default_menu);
        }

        
	}
	
	void Update () {

        float t = Time.time;
        if ( t >= end_time) {

            // - FINISHED TRANSITION -- //
            //Debug.Log("Transition over");
            transition.on_done.Invoke();

            enabled = false;

            //put everything back in its original place
            foreach (initial_fly_in_values_class initial_values in catalog) {
                RectTransform rect = initial_values.o.GetComponent<RectTransform>();
                rect.position = new Vector3(initial_values.x, initial_values.y, 0);
            }

            switch ( state) {
                case fly_in_menu_state.entering:
                    state = fly_in_menu_state.open;
                    if (close_was_called) {
                        //Debug.Log("Close was called while entering");
                        close();
                    }
                    if ( open_was_called) {
                        //Debug.Log("Open was called while entering");
                        open(next_page);

                    }
                    break;

                case fly_in_menu_state.exiting:
                    page.SetActive(false);
                    state = fly_in_menu_state.closed;
                    if (open_was_called) {
                        //Debug.Log("Open was called while exiting");
                        open(next_page);
                    }
                    if (close_was_called) {
                        //Debug.Log("Close was called while exiting");
                    }
                    break;
            }



        } else {
            
            foreach( initial_fly_in_values_class initial_values in catalog) {
                if ( t >= initial_values.start_time && !initial_values.fly_invoked ) {
                    initial_values.fly_invoked = true;
                    transition.on_fly_object.Invoke();
                    if (initial_values.is_exception) {

                        //check if the object has an exception component and invoke a fly in or fly-out unity event if it does.
                        fly_in_exception_handler handler = initial_values.o.GetComponent<fly_in_exception_handler>();
                        if ( handler) {
                            if (state == fly_in_menu_state.entering) {
                                handler.on_fly_in.Invoke();
                            } else {
                                handler.on_fly_out.Invoke();
                            }
                        }
                    }
                }

                if (!initial_values.is_exception) {

                    float n = (t - initial_values.start_time) / transition.speed;
                    RectTransform rect = initial_values.o.GetComponent<RectTransform>();

                    rect.position = new Vector3(initial_values.x + (transition.x_curve.Evaluate(n) * Screen.width), initial_values.y + (transition.y_curve.Evaluate(n) * Screen.height), 0);
                }

                //todo: apply curve to scale and rotation
                //rect.localScale.Set(initial_values.sx * transition.sx_curve.Evaluate(n), initial_values.sy * transition.sy_curve.Evaluate(n),1);
                //rect.rotation.eulerAngles.Set(0,0,transition.rotation_curve.Evaluate(n));
            }

        }


    }
}