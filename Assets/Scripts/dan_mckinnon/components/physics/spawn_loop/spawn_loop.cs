// Dan's Jam Pack for Unity #1.
// ---
// by Dan Player McKinnon
// E-mail: dan@danmckinnon.net
// Created: February 2016
// Description:
//		This asset pack gives some miscellanous boilerplate functionality
//		that are very handy when prototyping and jamming out games.
// Liscence:  
//		This work is licensed under the Creative Commons Attribution 4.0 International License. 
//		To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/.

// Spawn Loop
// ---
// Continously spawn game objects in a timed loop until depleted.
using UnityEngine;
using UnityEngine.Events;
using System.Collections;

[AddComponentMenu("Dan McKinnon/Physics/Spawn loop")]
public class spawn_loop : MonoBehaviour {

    [Tooltip("Immediately spawn the target game object once when this object is awoken.")]
    public bool spawn_on_awake = true;
    [Tooltip("The minimum interval between spawns in seconds.")]
    public float min_interval = 3;
    [Tooltip("The maximum interval between spawns in seconds")]
    public float max_interval = 3;
    [Tooltip("The maximum number of times this spawner should produce game objects.  Set to -1 for indefinately.")]
    public int how_many = -1;    // -1 = repeat infinitely
    private int remaining = -1;
    private float next_time = 3;

    [Tooltip("The gameObject to clone.")]
    public GameObject clone_what;
    [Tooltip("Triggered every time something is spawned.")]
    public UnityEvent on_spawn;



	void Start () {
        remaining = how_many;
        calculate_interval();
	}

    void Awake() {
        if ( spawn_on_awake) {
            do_spawn();
        }
    }
	
	void Update () {
	    if ( Time.time >= next_time) {
            do_spawn();
            if ( remaining > 0){
                remaining--;
                if ( remaining == 0){
                    enabled = false;                    
                }
            } 
        }
	}

	// reload()
    // ---
    // reload the object number of objects loaded in the spawner.

    public void reload(){
        remaining = how_many;
    }
    
    // reload( amount )
    // ---
    //    amount 		- the new amount of items loaded into the spawner.
    public void reload(int amount){
        how_many = amount;
        reload();
    }

    // get_remaining()
    // ---
    // returns the number of items loaded into the spawner
    public int get_remaining(){
        return remaining;
    }
    
    // do_spawn()
    // -- 
    // Force an item to spawn from the spawner.
    public void do_spawn() {

        if (clone_what != null) {
            GameObject o = (GameObject)Instantiate(clone_what, transform.position, Quaternion.identity);
            o.SetActive(true);
            on_spawn.Invoke();
            calculate_interval();
        }


    }
    private void calculate_interval() {
        next_time = Time.time + Random.Range(min_interval, max_interval);
    }

    void OnDrawGizmos() {
        Gizmos.color = Color.green;
        Gizmos.DrawIcon(transform.position, "spawn.png");
    }
}
