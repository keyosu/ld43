﻿// Dan's Jam Pack for Unity #1.
// ---
// by Dan Player McKinnon
// E-mail: dan@danmckinnon.net
// Created: February 2016
// Description:
//		This asset pack gives some miscellanous boilerplate functionality
//		that are very handy when prototyping and jamming out games.
// Liscence:  
//		This work is licensed under the Creative Commons Attribution 4.0 International License. 
//		To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/.

// Trigger (event)
// ---
// A basic trigger area for use with Unity Events.
// You must attach a collider set to trigger to any game object with this component as well.

using UnityEngine;
using System.Collections;
using UnityEngine.Events;
[AddComponentMenu("Dan McKinnon/Events/Trigger area")]
public class trigger : MonoBehaviour {

    [Tooltip("Triggered every time something enters the trigger area.")]
    public UnityEvent on_trigger;   //called when anything enters a trigger area
    [Tooltip("Triggered when the first item enters the trigger area.")]
    public UnityEvent on_enter;     //called when an empty trigger area is populated
    [Tooltip("Triggered when the last object inside the trigger area leaves")]
    public UnityEvent on_leave;     //called when nothing is left inside trigger area


    [HideInInspector]
    public int trigger_count = 0;
    void OnTriggerEnter2D(Collider2D c) {
        trigger_count++;
        if (trigger_count == 1) {
            on_enter.Invoke();
        }
        on_trigger.Invoke();
    }
    void OnTriggerEnter(Collider c) {
        on_enter.Invoke();
    }
    void OnTriggerExit2D(Collider2D c) {
        trigger_count--;
        if (trigger_count == 0) {;
            on_leave.Invoke();
        }
    }
    void OnTriggerExit(Collider c) {
        on_leave.Invoke();
    }


}
