﻿// Dan's Jam Pack for Unity #1.
// ---
// by Dan Player McKinnon
// E-mail: dan@danmckinnon.net
// Created: February 2016
// Description:
//		This asset pack gives some miscellanous boilerplate functionality
//		that are very handy when prototyping and jamming out games.
// Liscence:  
//		This work is licensed under the Creative Commons Attribution 4.0 International License. 
//		To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/.

// Paralax group menu
// ---
// A menu for adding paralax items.
//

using UnityEngine;
using System.Collections;
using UnityEditor;

public class paralax_group_menu_item {


    //
    // ---
    //
    //
    [MenuItem("GameObject/Dan McKinnon/Physics/Paralax group",false,10)]
    public static void add_paralax_group() {
        GameObject o = new GameObject("paralax_group");
        o.AddComponent<paralax>();
        if (Selection.activeGameObject) {
            o.transform.SetParent(Selection.activeGameObject.transform);
        }
        Undo.RegisterCompleteObjectUndo(o, "create paralax group");

    }

}
