﻿// Dan's Jam Pack for Unity #1.
// ---
// by Dan Player McKinnon
// E-mail: dan@danmckinnon.net
// Created: February 2016
// Description:
//		This asset pack gives some miscellanous boilerplate functionality
//		that are very handy when prototyping and jamming out games.
// Liscence:  
//		This work is licensed under the Creative Commons Attribution 4.0 International License. 
//		To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/.

// Paralax
// ---
// The paralax effect helps create depth to 2D games by allowing background or foreground layers to move
//
using UnityEngine;
using System.Collections;

[AddComponentMenu("Dan McKinnon/Physics/Paralax")]
public class paralax : MonoBehaviour {

    [Tooltip("A value of 1, the layer will move with the camera l(background effect).  A value of 0 will create no effect.  A value of -1 will make the object move faster than the camera (foreground effect)")]
    [Range(-1,1)]
    public float paralax_scale = 0.5f;
    private Vector3 start_position;


    public Camera cam = null;
    void Start() {
	    if (cam == null ){
            cam = Camera.main;
	    }
        start_position = transform.position;

    }

    void Update() {


	if (cam != null ){
		Vector3 difference = Vector3.Scale(cam.transform.position, new Vector3(paralax_scale, paralax_scale, 0));
		transform.position = start_position + difference;
	}
}























    //   public Vector2 paralax_scale = new Vector2(-5,-5);
    //   private Vector3 old_cam_position;
    //   private Camera cam;

    //// Use this for initialization
    //void Start () {
    //       cam = Camera.main;
    //       if ( cam == null) {
    //           Debug.LogWarning("You must have a main camera set for paralax layers to work");
    //           old_cam_position = Vector3.zero;
    //       } else {
    //           old_cam_position = cam.transform.position;

    //       }

    //   }

    //// Update is called once per frames
    //void Update () {
    //       if (cam != null) {
    //           float amount_x = (old_cam_position.x - cam.transform.position.x) * paralax_scale.x;
    //           float amount_y = (old_cam_position.y - cam.transform.position.y) * paralax_scale.y;
    //           Vector3 target = new Vector3(transform.position.x + amount_x, transform.position.y + amount_y, transform.position.z);
    //           transform.position = target;

    //           old_cam_position = cam.transform.position;
    //       }

    //   }
}
