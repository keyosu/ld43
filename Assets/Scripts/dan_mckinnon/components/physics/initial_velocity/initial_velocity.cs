﻿// Dan's Jam Pack for Unity #1.
// ---
// by Dan Player McKinnon
// E-mail: dan@danmckinnon.net
// Created: February 2016
// Description:
//		This asset pack gives some miscellanous boilerplate functionality
//		that are very handy when prototyping and jamming out games.
// Liscence:  
//		This work is licensed under the Creative Commons Attribution 4.0 International License. 
//		To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/.

// Initial velocity
// ---
// Set an initial velocity on a game object and visualize  it too.
// Requires a rigidbody component as well.
using UnityEngine;
using System.Collections;

[AddComponentMenu("Dan McKinnon/Physics/Initial velocity")]
public class initial_velocity : MonoBehaviour {

    [Tooltip("")]
    public Vector3 velocity = Vector3.right;

    private Vector3 start_location;

    void OnDrawGizmos() {
        Gizmos.color = Color.blue;

        Gizmos.DrawLine(transform.position, transform.position + velocity);
        Gizmos.DrawSphere(transform.position + velocity, 0.1f);
    }

    void Start() {
        restart();
    }
    void Awake() {
        restart();
    }

    // restart()
    // ---
    // apply the initial velocity again
    public void restart() {
        Rigidbody2D body2d = GetComponent<Rigidbody2D>();

        if ( body2d) {
            body2d.velocity = new Vector2(velocity.x, velocity.y);
        } else {
            Rigidbody body = GetComponent<Rigidbody>();
            if ( body) {
                body.velocity = velocity;
            } else {
                Debug.LogWarning("No rigidbody attached to GameObject.");
            }
        }
    }

}
