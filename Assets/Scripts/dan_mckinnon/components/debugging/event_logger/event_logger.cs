﻿using UnityEngine;
using System.Collections;

[AddComponentMenu("Dan McKinnon/Debugging/Event Logger")]
public class event_logger : MonoBehaviour {

    public void Log(string msg) {
        Debug.Log(gameObject.name + ": " + msg);
    }
}
