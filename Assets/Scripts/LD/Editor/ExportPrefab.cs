﻿using UnityEditor;
using UnityEngine;

namespace LD
{
    public class ExportPrefab : MonoBehaviour
    {

        [MenuItem("Tools/Create Prefab %e")]
        static void CreatePrefab()
        {
            var objs = Selection.gameObjects;
            for (int i = 0; i < objs.Length; i++)
            {
                var go = objs[i];
                string localPath = "Assets/Prefabs/" + go.name + ".prefab";
                Object prefab = AssetDatabase.LoadAssetAtPath(localPath, typeof(GameObject));

                if (prefab)
                {
                    PrefabUtility.ReplacePrefab(go, prefab, ReplacePrefabOptions.ReplaceNameBased);
                }
                else
                {
                    PrefabUtility.CreatePrefab(localPath, go);
                }
                AssetDatabase.Refresh();
            }

            Debug.Log("Finished Exporting!");
            EditorApplication.Beep();
        }

        [MenuItem("Tools/Create Prefab", true)]
        static bool ValidateCreatePrefab()
        {
            return Selection.activeGameObject != null;
        }

        static void CreateNew(GameObject obj, string localPath)
        {
            PrefabUtility.CreatePrefab(localPath, obj);
            AssetDatabase.Refresh();
        }

    }

}