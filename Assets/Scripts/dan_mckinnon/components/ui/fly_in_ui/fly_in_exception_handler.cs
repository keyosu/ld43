﻿// Dan's Jam Pack for Unity #1.
// ---
// by Dan Player McKinnon
// E-mail: dan@danmckinnon.net
// Created: February 2016
// Description:
//		This asset pack gives some miscellanous boilerplate functionality
//		that are very handy when prototyping and jamming out games.
// Liscence:  
//		This work is licensed under the Creative Commons Attribution 4.0 International License. 
//		To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/.

// Fly-in exception handler
// ---
// For use with fly-in menus.  This helps define a list of actions to take when an exception to the fly-in rule
// is encountered.    
using UnityEngine;
using System.Collections;
using UnityEngine.Events;
[AddComponentMenu("Dan McKinnon/UI/Fly-in exception handler")]
public class fly_in_exception_handler : MonoBehaviour {

    [Tooltip("Triggered when the object would normally be queued to fly in")]
    public UnityEvent on_fly_in;
    [Tooltip("Triggered when the object would normally be queued to fly out")]
    public UnityEvent on_fly_out;

}
