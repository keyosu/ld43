﻿// Dan's Jam Pack for Unity #1.
// ---
// by Dan Player McKinnon
// E-mail: dan@danmckinnon.net
// Created: February 2016
// Description:
//		This asset pack gives some miscellanous boilerplate functionality
//		that are very handy when prototyping and jamming out games.
// Liscence:  
//		This work is licensed under the Creative Commons Attribution 4.0 International License. 
//		To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/.

// Digital distortion menu
// ---
// Adds digital distortion to a camera.  
//
using UnityEngine;
using System.Collections;
using UnityEditor;
using System.IO;
public class distortion_camera_editor  {

    //
    // ---
    //
    //
    [MenuItem("GameObject/Dan McKinnon/Camera FX/Digital distortion",false,10)]


    //create copy of material (if applicable)
    //create camera (if applicable)
    //attach filter



    //aquire camera
    //-------------
    //look for a main camera
    //if there is one,  
    // ask the user if they'd rather attach this to the main camera instead
    // if yes..
    //cam = main_camera
    //if cam== null
    // create a camera
    // cam = new camera
    // if there's a 
    //selection parent it to selection


    //aquire filter
    //-------------
    //if there is already a filter attached...
    //ask the player if they'd like to change it..
    //yes...
    //add filter to cam
    //no.. return;



    //aquire material
    //---------------
    //Ask about creating a new one
    //if so,  copy the file and select the new material
    //else, use the built-in one.


    //attach material
    //---------------


    public static void add_digital_distortion() {
        GameObject cam = null;

        bool created_new_cam = false;
        //aquire camera
        if (Camera.main) {
            bool yes_main_cam = EditorUtility.DisplayDialog("Add distortion to main camera.", "Would you rather add a digital distortion filter to the main camera instead?", "Yes", "No");
            if (yes_main_cam) {
                cam = Camera.main.gameObject;
            }
        }

        if (cam == null) {
            cam = new GameObject("digital_distortion_camera");
            cam.AddComponent<Camera>();
            cam.AddComponent<AudioListener>();
            cam.AddComponent<GUILayer>();
            cam.tag = "MainCamera";
            if (Selection.activeGameObject) {
                cam.transform.SetParent(Selection.activeGameObject.transform);
            }
            created_new_cam = true;
        }

        camera_filter filter = cam.GetComponent<camera_filter>();
        if (filter != null) {
            bool change_filter = EditorUtility.DisplayDialog("Filter already attached", "There is a filter already attached,  would you like to change filter to distortion?", "Yes", "No");
            if (!change_filter) {
                EditorUtility.DisplayDialog("Change cancelled", "Digital distortion camera filter change cancelled.", "Ok");
                return;
            }
        } else {
            if (!created_new_cam) {
                filter = Undo.AddComponent<camera_filter>(cam);
            } else {
                filter = cam.gameObject.AddComponent<camera_filter>();
            }
        }

        //aquire material
        bool yes_create_material = EditorUtility.DisplayDialog("Create new material?", "Create a new material to customize the digital distortion with?", "Yes", "No");
        Material mat;
        if (!created_new_cam) {
            Undo.RegisterCompleteObjectUndo(cam, "change camera material");
        }
        if (yes_create_material) {
            string path = AssetDatabase.GenerateUniqueAssetPath("Assets/new_digital_distortion_material.mat");
            AssetDatabase.CopyAsset("Assets/dan_mckinnon/components/camera_fx/digital_distortion/digital_distortion_material.mat", path);
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
            mat = AssetDatabase.LoadAssetAtPath<Material>(path);
        } else {
            mat = AssetDatabase.LoadAssetAtPath<Material>("Assets/dan_mckinnon/components/camera_fx/digital_distortion/digital_distortion_material.mat");
        }

        filter.material_for_shader = mat;

        if (created_new_cam) {
            Undo.RegisterCompleteObjectUndo(cam, "create digital distortion camera");
        }
    }


    //
    // ---
    //
    //
    [MenuItem("Assets/Create/Dan McKinnon/Digital distortion material", false, 10)]
    public static void add_masked_lighting_material() {
        string path = AssetDatabase.GetAssetPath(Selection.activeObject);
        if (path == "") {
            path = "Assets";
        } else if (Path.GetExtension(path) != "") {
            path = path.Replace(Path.GetFileName(AssetDatabase.GetAssetPath(Selection.activeObject)), "");
        }

        path = AssetDatabase.GenerateUniqueAssetPath(path + "/new_digital_distortion_material.mat");
        AssetDatabase.CopyAsset("Assets/dan_mckinnon/components/camera_fx/digital_distortion/digital_distortion_material.mat", path);
        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();
    }


}
