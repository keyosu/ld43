﻿using UnityEngine;

namespace LD
{
    [System.Serializable]
    public class Equip : ScriptableObject
    {
        public string Name;
        public string Description;

        public int Power;
    }
}
