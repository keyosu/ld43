﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "DanMcKinnon / CameraFX / Masked Lighting"
{
	Properties
	{
		_MainTex("Texture", 2D) = "clear" {}
		_lighting( "Texture",2D) = "clear" {}
		_ambient_colour("Ambient colour", Color) = (0.1, 0.1, 0.35, 0.5) // color		
		_alpha("Light Alpha",range(0,1)) = 0.25
		_sharpness("Sharpness",range(0,5)) = 2
		[Toggle] _flip_lighting_layer("Flip lighting layer", Float) = 0
		

	}
	SubShader
	{
		// No culling or depth
		Cull Off ZWrite Off ZTest Always
		Blend SrcAlpha OneMinusSrcAlpha // Alpha blending

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;

			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			uniform half4 _MainTex_TexelSize;
			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = v.uv;



				return o;

			}
			
			sampler2D _MainTex;
			sampler2D _lighting;
			float4 _ambient_colour;
			float _alpha;
			float _sharpness;
			float _flip_lighting_layer;

			fixed4 frag (v2f i) : SV_Target
			{

				
				//NOTE: flipping this because for some reason relatex to DX vs GL
				//one texture gets flipped while the rendertexture does not.
				//I haven't the foggiest how to detect when and why,  though I tried...
				//you'll just have to set it manually with _flip_lighting_layer
				float2 uv_corrected = i.uv;
				if ( _flip_lighting_layer != 0 ){
					uv_corrected.y = 1 - uv_corrected.y;
				}

				fixed4 input_a = tex2D(_MainTex, i.uv);
				fixed4 input_b = tex2D(_lighting, uv_corrected );
				fixed4 output;

				output = lerp(input_a, _ambient_colour, _ambient_colour.a);
				output = lerp( output, input_a, input_b.a * _sharpness);
				output = lerp( output, input_b, input_b.a * _alpha);
				output.a = 1;
				
				return output;
			}
			ENDCG
		}
	}
}
