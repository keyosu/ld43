﻿using UnityEditor;
using UnityEngine;

namespace LD
{
    public class CreateSkill
    {
        [MenuItem("Assets/Create/Skill")]
        public static Skill Create()
        {
            Skill asset = ScriptableObject.CreateInstance<Skill>();

            AssetDatabase.CreateAsset(asset, "Assets/Data/Skills/Skill.asset");
            AssetDatabase.SaveAssets();
            return asset;
        }
    }
}