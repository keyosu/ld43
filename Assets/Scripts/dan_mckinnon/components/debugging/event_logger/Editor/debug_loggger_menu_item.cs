﻿// Dan's Jam Pack for Unity #1.
// ---
// by Dan Player McKinnon
// E-mail: dan@danmckinnon.net
// Created: February 2016
// Description:
//		This asset pack gives some miscellanous boilerplate functionality
//		that are very handy when prototyping and jamming out games.
// Liscence:  
//		This work is licensed under the Creative Commons Attribution 4.0 International License. 
//		To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/.

// Debug Logger Menu
// ---
// Conveniently add a game object with an event logger.
//
using UnityEngine;
using System.Collections;
using UnityEditor;

public class debug_loggger_menu_item {
    //
    // ---
    //
    //
    [MenuItem("GameObject/Dan McKinnon/Debugging/Event logger", false, 10)]
    public static void add_debug_logger() {
        GameObject o = new GameObject("event_logger");
        o.AddComponent<event_logger>();
        if (Selection.activeGameObject) { 
            o.transform.SetParent(Selection.activeGameObject.transform);
        }
        Undo.RegisterCreatedObjectUndo(o,"create event logger");
    }
    
}
