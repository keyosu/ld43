﻿// Dan's Jam Pack for Unity #1.
// ---
// by Dan Player McKinnon
// E-mail: dan@danmckinnon.net
// Created: February 2016
// Description:
//		This asset pack gives some miscellanous boilerplate functionality
//		that are very handy when prototyping and jamming out games.
// Liscence:  
//		This work is licensed under the Creative Commons Attribution 4.0 International License. 
//		To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/.

// Sinusoidal movement
// ---
// A quick way to make objects move in a sinusoidal pattern.  
// This is a good solution for creating moving platforms or obstacles.
//
using UnityEngine;
using System.Collections;

[AddComponentMenu("Dan McKinnon/Physics/Sinusoidal movement")]
public class sin_movement : MonoBehaviour {
    public enum floating_axis { x, y, z };
    [Tooltip("Revolutions per second")]
    public float speed = 1.0f;
    [Tooltip("The radius the object travels")]
    public float distance = 0.5f;

    [Tooltip("How far along the motion the object starts.  0.5 effectively changes starting direction,  0.25 effectively changes to cosine")]
    [Range(0,1)]
    public float offset = 0;
    [Tooltip("Which axis to move along")]
    public floating_axis axis;
    private Vector3 start;


    // Use this for initialization
    void Start () {
        start = transform.position;
       
	}
	
	// Update is called once per frame
    private Vector3 get_dir() {
        Vector3 dir;
        switch (axis) {
            case floating_axis.x:
                dir = Vector3.right;
                break;
            default:
            case floating_axis.y:
                dir = Vector3.up;
                break;
            case floating_axis.z:
                dir = Vector3.forward;
                break;
        }
        return dir;
    }
	void Update () {
        float offset_n = offset * 2 * Mathf.PI;
        float  n =  Mathf.Sin(offset_n + (Time.time* speed)) * distance;
        Vector3 dir = get_dir();
        transform.position = start + (n * dir);

	}

    void OnDrawGizmos() {
        Vector3 dir = get_dir();
        Vector3 v = (dir * distance);
        Gizmos.color = Color.blue;
        Gizmos.DrawLine(transform.position - v , transform.position + v);

        float offset_n = offset * 2 * Mathf.PI;
        float n = Mathf.Sin(offset_n) * distance;
        Gizmos.DrawSphere(transform.position + (dir * n),0.1f);

    }
}
