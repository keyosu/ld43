﻿// Dan's Jam Pack for Unity #1.
// ---
// by Dan Player McKinnon
// E-mail: dan@danmckinnon.net
// Created: February 2016
// Description:
//		This asset pack gives some miscellanous boilerplate functionality
//		that are very handy when prototyping and jamming out games.
// Liscence:  
//		This work is licensed under the Creative Commons Attribution 4.0 International License. 
//		To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/.

// Camera filter
// ---
// Used for special shader FX such as lighting, distortion, bloom, etc.
// Set and use _MainTex in your shader in order to access the screen.
using UnityEngine;
using System.Collections;


[AddComponentMenu("Dan McKinnon/Camera FX/Camera filter")]
public class camera_filter : MonoBehaviour {

    [Tooltip("A material that controls the shader parameters.")]
    public Material material_for_shader;
    void OnRenderImage (RenderTexture source, RenderTexture destination){
        Graphics.Blit(source,destination,material_for_shader);
    }

}
