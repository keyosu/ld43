﻿// Dan's Jam Pack for Unity #1.
// ---
// by Dan Player McKinnon
// E-mail: dan@danmckinnon.net
// Created: February 2016
// Description:
//		This asset pack gives some miscellanous boilerplate functionality
//		that are very handy when prototyping and jamming out games.
// Liscence:  
//		This work is licensed under the Creative Commons Attribution 4.0 International License. 
//		To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/.

// Modular song menu item
// ---
// Everything you need to play modular music
//
using UnityEngine;
using System.Collections;
using UnityEditor;
using UnityEditor.Events;
using UnityEngine.Events;

public class modular_song_menu_item {
    //
    // ---
    //
    //
    [MenuItem("GameObject/Dan McKinnon/Music/Modular song", false, 10)]
    public static GameObject add_song() {
        GameObject o = new GameObject("modular_song");
        module_song song = o.AddComponent<module_song>();
        if (Selection.activeGameObject) {
            o.transform.SetParent(Selection.activeGameObject.transform);
        }
        AudioSource src;
        song.repeat_position = 1;
        GameObject intro = new GameObject("intro");
        src = intro.AddComponent<AudioSource>();
        src.loop = false;
        src.playOnAwake = false;
        intro.transform.SetParent(o.transform);


        GameObject verse = new GameObject("verse");
        src = verse.AddComponent<AudioSource>();
        src.loop = false;
        src.playOnAwake = false;
        verse.transform.SetParent(o.transform);

        GameObject chorus = new GameObject("chorus");
        src = chorus.AddComponent<AudioSource>();
        src.loop = false;
        src.playOnAwake = false;
        chorus.transform.SetParent(o.transform);

        Undo.RegisterCreatedObjectUndo(o, "create modular song");
        return o;
    }

    //
    // ---
    //
    //
    [MenuItem("GameObject/Dan McKinnon/Music/Modular song player", false, 10)]
    public static GameObject add_player() {
        GameObject o = new GameObject("modular_song_player");
        o.AddComponent<module_song_player>();
        if (Selection.activeGameObject) {
            o.transform.SetParent(Selection.activeGameObject.transform);
        }

        Undo.RegisterCreatedObjectUndo(o, "create modular song player");
        return o;

    }

    
    
    //
    // ---
    //
    //
    [MenuItem("GameObject/Dan McKinnon/Music/Modular song + play on begin",false,10)]
    public static void add_both() {
        GameObject player = add_player();
        GameObject song = add_song();
        module_song s = song.GetComponent<module_song>();
        module_song_player p = player.GetComponent<module_song_player>();
        begin b = player.AddComponent<begin>();
        UnityAction<module_song> action = new UnityAction<module_song>(p.play);
        b.on_scene_begin = new UnityEvent();
        UnityEventTools.AddObjectPersistentListener(b.on_scene_begin, action, s);

        Undo.RegisterCreatedObjectUndo(song, "create modular song");
        Undo.RegisterCreatedObjectUndo(player, "create modular song player");
    }
}
