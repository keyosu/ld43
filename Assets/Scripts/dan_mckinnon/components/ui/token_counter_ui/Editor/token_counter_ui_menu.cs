﻿// Dan's Jam Pack for Unity #1.
// ---
// by Dan Player McKinnon
// E-mail: dan@danmckinnon.net
// Created: February 2016
// Description:
//		This asset pack gives some miscellanous boilerplate functionality
//		that are very handy when prototyping and jamming out games.
// Liscence:  
//		This work is licensed under the Creative Commons Attribution 4.0 International License. 
//		To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/.

// UI Counter Menu
// ---
// Conveniently add a token counter game object to the scene
using UnityEngine;
using System.Collections;
using UnityEditor;
public class token_counter_ui_menu {
    //
    // ---
    //
    //
    [MenuItem("GameObject/Dan McKinnon/UI/Token counter",false,10)]
    public static void add_token_counter_menu() {
        GameObject o = new GameObject("token_counter");
        o.AddComponent<RectTransform>();
        o.AddComponent<token_counter_ui>();
        if ( Selection.activeGameObject != null) {
            o.transform.SetParent(Selection.activeGameObject.transform);
        }
        EditorUtility.DisplayDialog("Setup token gameobjects", "Note: you must add on_image and off_image icons of your new token counter for it to become visible", "Ok");
        Undo.RegisterCreatedObjectUndo(o, "create token counter");
    }


}
