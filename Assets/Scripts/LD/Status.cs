﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class Status : MonoBehaviour
{
    public enum StatusType
    {
        None, Guard, Provoke, DefDown, DefUp, AtkDown, AtkUp, SpdDown, SpdUp, Stun, Drain, Bleed, Confuse,
        Dispel, IgnoreDef, Stack
    }

    public StatusType statusType;
    public Image StatusIcon;
    public TextMeshProUGUI StatusDuration;
    public int RemainingTurnCount = 1;

    public void Assign(Sprite sprite, StatusType type, int duration)
    {
        statusType = type;
        StatusIcon.sprite = sprite;
        RemainingTurnCount = duration;
        StatusDuration.text = RemainingTurnCount.ToString();
        gameObject.SetActive(true);
    }

    public void Progress()
    {
        RemainingTurnCount -= 1;
        if (!IsActive)
        {
            SimplePool.Despawn(gameObject);
        }
        else
        {
            StatusDuration.text = RemainingTurnCount.ToString();
        }
    }

    public void End()
    {
        SimplePool.Despawn(gameObject);
    }

    public bool IsActive
    {
        get
        {
            return RemainingTurnCount > 0;
        }
    }
}
