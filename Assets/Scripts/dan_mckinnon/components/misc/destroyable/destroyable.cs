﻿using UnityEngine;
using System.Collections;

[AddComponentMenu("Dan McKinnon/Misc/Destroyable")]
public class destroyable : MonoBehaviour {

	// Use this for initialization
	void Start () {
        enabled = false;
	}
	

    public void destroy() {
        Destroy(gameObject);
    }
    public void destroy_delayed(float delay) {
        Invoke("destroy", delay);
    }
}
