﻿// Dan's Jam Pack for Unity #1.
// ---
// by Dan Player McKinnon
// E-mail: dan@danmckinnon.net
// Created: February 2016
// Description:
//		This asset pack gives some miscellanous boilerplate functionality
//		that are very handy when prototyping and jamming out games.
// Liscence:  
//		This work is licensed under the Creative Commons Attribution 4.0 International License. 
//		To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/.

// Begin (event)
// ---
// Triggered when the scene begins.   

using UnityEngine;
using System.Collections;
using UnityEngine.Events;

[AddComponentMenu("Dan McKinnon/Events/On begin")]
public class begin : MonoBehaviour {

    [Tooltip("The amount of delay (in seconds) after the scene begins.")]
    public float delay = 0;
    [Tooltip("Triggered when the scene begins, after the delay specified above.")]
    public UnityEvent on_scene_begin;

    void Start() {
        Invoke("go", delay);
    }
    void go() {
        on_scene_begin.Invoke();
    }

}
