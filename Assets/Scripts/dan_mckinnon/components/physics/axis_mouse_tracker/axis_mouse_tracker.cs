﻿// Dan's Jam Pack for Unity #1.
// ---
// by Dan Player McKinnon
// E-mail: dan@danmckinnon.net
// Created: February 2016
// Description:
//		This asset pack gives some miscellanous boilerplate functionality
//		that are very handy when prototyping and jamming out games.
// Liscence:  
//		This work is licensed under the Creative Commons Attribution 4.0 International License. 
//		To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/.

// axis Mouse Tracker
// ---
// 1 part physics, 1 part input.   This object will follow the mouse on the axis specified.
// You can also restrict the range of movement with a minimum and maximum value
// This component requires a Main Camera to function.
using UnityEngine;
using System.Collections;

[AddComponentMenu("Dan McKinnon/Physics/Track mouse on axis")]
public class axis_mouse_tracker : MonoBehaviour {

    [Tooltip("The axis to move along.")]
    public axis_type axis;


    [Tooltip("The maximum position along the axis.  The object will stop tracking the mouse beyond this value.  Can conveniently be set visually using the editor.")]
    public float max_val = 10.0f;
    [Tooltip("The minimum position along the axis.  The object will stop tracking the mouse before it reaches this value. Can conveniently be set visually using the editor.")]
    public float min_val = 0.0f;

    private Vector3 start_position;

    void Start() {




        start_position = transform.position;
    }

    void Update() {
        float xx = Input.mousePosition.x;
        float yy = Input.mousePosition.y;
        if (Camera.main != null) {
            Vector3 pos = Camera.main.ScreenToWorldPoint(new Vector3(xx, yy, 10.0f));

            float n;
            switch (axis) {
                case axis_type.x:                    
                    n = Mathf.Clamp(pos.x, min_val, max_val );
                    transform.position = new Vector3(n, start_position.y, start_position.z);
                    break;
                case axis_type.y:
                    n = Mathf.Clamp(pos.y, min_val , max_val );
                    transform.position = new Vector3(start_position.x, n, start_position.z);
                    break;

            }
        }
    }

    void OnDrawGizmosSelected() {
        Gizmos.color = Color.blue;
        switch (axis) {
            case axis_type.x:
                Gizmos.DrawLine(transform.position, new Vector3(max_val, transform.position.y, transform.position.z));
                Gizmos.DrawLine(transform.position, new Vector3(min_val, transform.position.y, transform.position.z));
                break;
            case axis_type.y:
                Gizmos.DrawLine(transform.position, new Vector3(transform.position.x, max_val, transform.position.z));
                Gizmos.DrawLine(transform.position, new Vector3(transform.position.x, min_val, transform.position.z));
                break;
        }
    }
}