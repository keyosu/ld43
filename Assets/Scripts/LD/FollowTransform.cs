﻿using UnityEngine;

public class FollowTransform : MonoBehaviour
{

    public Transform Target;
    private float offset;

    private void Start()
    {
        offset = transform.position.x - Target.position.x;
    }
    private void LateUpdate()
    {
        transform.position = new Vector3(Target.position.x + offset, Target.position.y, transform.position.z);
    }
}
