﻿// Dan's Jam Pack for Unity #1.
// ---
// by Dan Player McKinnon
// E-mail: dan@danmckinnon.net
// Created: February 2016
// Description:
//		This asset pack gives some miscellanous boilerplate functionality
//		that are very handy when prototyping and jamming out games.
// Liscence:  
//		This work is licensed under the Creative Commons Attribution 4.0 International License. 
//		To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/.

// Empty
// ---
// Triggers when a gameobject has lost all its children.
//
using UnityEngine;
using System.Collections;
using UnityEngine.Events;
[AddComponentMenu("Dan McKinnon/Events/On empty")]
public class empty : MonoBehaviour {

    [Tooltip("How often to check in seconds.")]
    public float check_interval = 0.1f;
    [Tooltip("What to do when this game object is empty.")]
    public UnityEvent on_empty;
	// Use this for initialization
	void Start () {
        enabled = false;
        InvokeRepeating("do_check", check_interval, check_interval);

    }
	
    // do_check()
    // ---
    // Force a check between intervals.
	public void do_check () {
        
	    if ( transform.childCount <= 0) {
            CancelInvoke("do_check");
            on_empty.Invoke();

        }
	}
}
