﻿using DG.Tweening;
using MEC;
using Sirenix.OdinInspector;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace LD
{
    public class BattleManager : MonoBehaviour
    {
        public static BattleManager instance;

        public Entity CurrentEntity;
        public Entity CurrentTarget;

        [Space(5)]
        public Canvas BattleCanvas;
        public Canvas TravelCanvas;
        public Canvas OverlayCanvas;
        public RectTransform Commands;

        [Space(10)]
        public Skill AttackSkill;
        public Skill GuardSkill;
        public HoverUI TargetIndicator;
        public TextMeshProUGUI ActionInfo;
        public TextMeshProUGUI SkillInfo;
        public bool TargetAlly = false;

        [Space(5)]
        [SerializeField]
        private List<Sprite> statusSprites = new List<Sprite>();

        [Space(10)]
        public GameObject StatusPrefab;
        public GameObject DamagePrefab;

        public GameObject ItemUsagePrefab;
        public GameObject SkillUsagePrefab;

        [Space(5)]
        public GameObject ActionContainer;
        public GameObject ItemContainer;
        public GameObject SkillContainer;

        [Space(10)]
        [SerializeField]
        private List<HoverUI> hoverGroups = new List<HoverUI>();
        [SerializeField]
        private List<RectTransform> turnTracks = new List<RectTransform>();

        private List<Entity> allyEntities = new List<Entity>();
        private List<Entity> enemyEntities = new List<Entity>();
        private List<Entity> allEntities = new List<Entity>();

        private List<GameObject> spawnedSkills = new List<GameObject>();

        private void Awake()
        {
            instance = this;
        }

        public void Initialize(Entity entity)
        {
            for (int i = 0; i < turnTracks.Count; i++)
            {
                if (!turnTracks[i].gameObject.activeSelf)
                {
                    entity.TurnTrack = turnTracks[i];
                    entity.TurnTrack.GetComponentInChildren<Image>().sprite = entity.Icon;
                    entity.TurnTrack.gameObject.SetActive(true);
                    break;
                }
            }

            for (int i = 0; i < hoverGroups.Count; i++)
            {
                if (!hoverGroups[i].gameObject.activeSelf)
                {
                    entity.HoverGroup = hoverGroups[i];
                    hoverGroups[i].Assign(entity);
                    break;
                }
            }

            if (entity.IsPlayer)
            {
                allyEntities.Add(entity);
            }
            else
            {
                enemyEntities.Add(entity);
            }
            allEntities.Add(entity);
        }


        #region --- Battle Flow ---

        [Button]
        public void InitiateBattle()
        {
            DisableCommands(false);
            SelectTarget(enemyEntities[0]);

            OverlayCanvas.gameObject.SetActive(true);
            BattleCanvas.gameObject.SetActive(true);
            TravelCanvas.gameObject.SetActive(false);
            for (int i = 0; i < allyEntities.Count; i++)
            {
                allyEntities[i].TurnProgress = 50 + allyEntities[i].SPEED * Random.Range(0.95f, 1f);
                allyEntities[i].CurrentHP -= 0;
                allyEntities[i].CurrentMP -= 0;
            }
            for (int i = 0; i < enemyEntities.Count; i++)
            {
                enemyEntities[i].TurnProgress = enemyEntities[i].SPEED;
                enemyEntities[i].CurrentHP -= 0;
                enemyEntities[i].CurrentMP -= 0;
            }
            Timing.RunCoroutine(_NextTurn(false));
        }

        [Button]
        public void EndBattle()
        {
            OverlayCanvas.gameObject.SetActive(false);
            BattleCanvas.gameObject.SetActive(false);
            TravelCanvas.gameObject.SetActive(true);
            MainManager.instance.EndAction();
            PlayMusic.instance.PlaySelectedMusic(MainManager.instance.LevelMusic);
        }

        [Button]
        public void NextTurn()
        {
            HideAllContainers();
            UpdateActionInfo(string.Empty);
            Timing.RunCoroutine(_NextTurn(true));
        }

        private IEnumerator<float> _NextTurn(bool tween)
        {
            if (CurrentEntity != null)
            {
                CurrentEntity.TurnTrack.anchoredPosition = TrackPosition(CurrentEntity.TurnProgress);
            }
            CurrentEntity = null;
            while (CurrentEntity == null)
            {
                for (int i = 0; i < allEntities.Count; i++)
                {
                    Entity entity = allEntities[i];
                    if (CurrentEntity == null && entity.UpdateTurnTrack())
                    {
                        CurrentEntity = entity;
                        break;
                    }
                }
                yield return 0f;
            }
            for (int i = 0; i < allEntities.Count; i++)
            {
                Entity entity = allEntities[i];
                if (tween)
                {
                    entity.TurnTrack.DOAnchorPos(TrackPosition(entity.TurnProgress), 0.5f);
                }
                else
                {
                    entity.TurnTrack.anchoredPosition = TrackPosition(entity.TurnProgress);
                }
            }
            CurrentEntity.UpdateTurn();

            if (CurrentEntity.IsPlayer)
            {
                EnableCommands();
                if (!TargetIndicator.gameObject.activeSelf)
                {
                    TargetIndicator.gameObject.SetActive(true);
                }
                if (CurrentTarget == null)
                {
                    CurrentTarget = enemyEntities[0];
                }
                TargetIndicator.Assign(CurrentTarget);
            }
            else
            {
                DisableCommands(true);
                yield return Timing.WaitForSeconds(0.5f);
                Think();
            }
        }

        #endregion


        #region --- UI ---

        public void HideAllContainers()
        {
            ActionContainer.SetActive(false);
            ItemContainer.SetActive(false);
            SkillContainer.SetActive(false);
        }

        public void ShowActions()
        {
            HideAllContainers();
            ActionContainer.SetActive(true);
        }

        public void ShowItems()
        {
            HideAllContainers();
            ItemContainer.SetActive(true);
        }

        public void ShowSkills()
        {
            HideAllContainers();
            SkillContainer.SetActive(true);
        }

        public void ShowSkillInfo(string text)
        {
            SkillInfo.transform.parent.gameObject.SetActive(true);
            SkillInfo.text = text;
        }

        public void HideSkillInfo()
        {
            SkillInfo.transform.parent.gameObject.SetActive(false);
        }

        public void UpdateActionInfo(string text)
        {
            ActionInfo.text = text;
        }

        public void SelectTarget(Entity target)
        {
            TargetIndicator.Assign(target);
            CurrentTarget = target;
        }

        private void EnableCommands()
        {
            Commands.DOAnchorPos(new Vector2(0, 0), 0.25f);
            for(int i = 0; i < spawnedSkills.Count; i++)
            {
                SimplePool.Despawn(spawnedSkills[i]);
            }

            spawnedSkills.Clear();
            for (int i = 2; i < CurrentEntity.skills.Count; i++)
            {
                GameObject g = SimplePool.Spawn(SkillUsagePrefab);
                g.GetComponentInChildren<CommandUI>().skill = CurrentEntity.skills[i];
                g.GetComponentInChildren<CommandUI>().Title.text = CurrentEntity.skills[i].Name;
                g.GetComponentInChildren<CommandUI>().Alt.text = (CurrentEntity.skills[i].Cost * 3).ToString();
                g.transform.SetParent(SkillContainer.transform);
                spawnedSkills.Add(g);
            }
        }

        private void DisableCommands(bool tween)
        {
            HideAllContainers();
            if (tween)
            {
                Commands.DOAnchorPos(new Vector2(-200, 0), 0.25f);
            }
            else
            {
                Commands.anchoredPosition = new Vector2(-200, 0);
            }
        }

        #endregion


        #region --- Actions ---

        public void AttackCommand()
        {
            PerformSkill(CurrentEntity, CurrentTarget, AttackSkill);
        }

        public void GuardCommand()
        {
            PerformSkill(CurrentEntity, CurrentEntity, GuardSkill);
        }

        #endregion


        #region --- Damage Calculation ---

        public void PlayerPerformSkill(Skill skill)
        {
            CurrentEntity.CurrentMP -= skill.Cost * 3;
            switch (skill.Targeting)
            {
                case Skill.Target.Self:
                    PerformSkill(CurrentEntity, CurrentEntity, skill);
                    break;
                case Skill.Target.Ally:
                    Entity lowestAlly = allyEntities[0];
                    if (allyEntities[1].CurrentHP < lowestAlly.CurrentHP)
                    {
                        lowestAlly = allyEntities[1];
                    }
                    PerformSkill(CurrentEntity, lowestAlly, skill);
                    break;
                case Skill.Target.AllAllies:
                    for (int i = 0; i < allyEntities.Count; i++)
                    {
                        PerformSkill(CurrentEntity, allyEntities[i], skill);
                    }
                    break;
                case Skill.Target.Enemy:
                    PerformSkill(CurrentEntity, CurrentTarget, skill);
                    break;
                case Skill.Target.AllEnemies:
                    for (int i = 0; i < enemyEntities.Count; i++)
                    {
                        PerformSkill(CurrentEntity, enemyEntities[i], skill);
                    }
                    break;
            }
        }

        public void PerformSkill(Entity user, Entity target, Skill skill)
        {
            user.AnimationSkill(target, skill);
            ShowSkillInfo(skill.Name);
            DisableCommands(true);

            Timing.RunCoroutine(_PerformSkill(user, target, skill));
        }
        public IEnumerator<float> _PerformSkill(Entity user, Entity target, Skill skill)
        {
            // Wait until skill animation is done
            while (user.InAnimation)
            {
                yield return 0f;
            }

            int damage = 0;
            bool missed = false;
            string textToShow = string.Empty;

            // Calculate and apply damage
            if (!skill.IsMagic && skill.Power > 0 && skill.Accuracy > 0)
            {
                if (CanHit(user.ACCURACY, target.EVASION, skill.Accuracy))
                {
                    damage = CalculateDamage(user, target, skill);
                }
                else
                {
                    missed = true;
                }
            }
            else if (skill.IsMagic)
            {
                damage = CalculateDamage(user, target, skill);
            }

            // Change damage if recovery
            if (skill.IsRecovery)
            {
                damage = Mathf.RoundToInt(user.MATK * skill.Power / 100f * Random.Range(0.95f, 1f));
            }

            if (skill.Targeting == Skill.Target.Enemy ||
                skill.Targeting == Skill.Target.AllEnemies)
            {
                target.CurrentHP -= damage;
                if (user.HasStatus(Status.StatusType.Drain))
                {
                    user.CurrentHP += Mathf.CeilToInt(damage / 2f);
                }
            }
            else if (skill.Targeting == Skill.Target.Ally ||
                skill.Targeting == Skill.Target.AllAllies)
            {
                target.CurrentHP += damage;
            }

            // Apply any statuses
            if (!missed)
            {
                ApplyStatus(target, skill.TargetStatus, skill.TargetDuration, skill.TargetPercentage);
            }
            ApplyStatus(user, skill.UserStatus, skill.UserDuration, skill.UserPercentage);

            // Get damage text
            if (missed)
            {
                textToShow = "<color=red>MISS!</color>";
            }
            if (damage > 0)
            {
                if (skill.Targeting == Skill.Target.Ally ||
                    skill.Targeting == Skill.Target.AllAllies)
                {
                    textToShow = "<color=green>+";
                }
                textToShow += damage.ToString();
            }

            // Display damage text
            HoverUI hoverUI = SimplePool.Spawn(DamagePrefab).GetComponent<HoverUI>();
            hoverUI.transform.SetParent(OverlayCanvas.transform);
            hoverUI.Assign(target);
            hoverUI.OtherValue.text = textToShow;
            hoverUI.SetLifetime(1f);

            // Debugging
            Debug.LogFormat("Used {0} on {1} for {2} damage!", skill.Name, target.name, textToShow);

            // Check victory conditions
            if (target.IsDead())
            {
                target.Death();
                allEntities.Remove(target);
                if (!target.IsPlayer)
                {
                    enemyEntities.Remove(target);
                    for (int i = 0; i < allyEntities.Count; i++)
                    {
                        allyEntities[i].CurrentExp += target.LVL * 20;
                        MainManager.instance.Coins += target.LVL * Random.Range(1, 4);
                    }
                    CheckVictory();
                }
                else
                {
                    allyEntities.Remove(target);
                    CheckDefeat();
                }
                if (CurrentTarget == target && enemyEntities.Count > 0)
                {
                    SelectTarget(enemyEntities[0]);
                }
                else
                {
                    TargetIndicator.gameObject.SetActive(false);
                }
            }

            // Proceed to next turn if necessary
            yield return Timing.WaitForSeconds(1.25f);
            HideSkillInfo();

            yield return Timing.WaitForSeconds(0.25f);
            NextTurn();
        }

        public int CalculateDamage(Entity user, Entity target, Skill skill)
        {
            float damage = 0f;

            float attack = skill.IsMagic ? user.MATK : user.PATK;
            float defence = skill.IsMagic ? target.MDEF : target.PDEF;

            if (skill.TargetStatus == Status.StatusType.IgnoreDef)
            {
                defence = 0;
            }
            float finalModifier = target.IsGuarding ? 0.5f : 1f;

            damage = (attack - defence) * skill.Power / 100f * Random.Range(0.95f, 1f) * finalModifier;
            return Mathf.RoundToInt(Mathf.Max(damage, 1));
        }

        public bool CanHit(float acc, float eva, int skillAcc)
        {
            float hitPercentage = 100 + ((acc * skillAcc / 100f) - eva);
            if (hitPercentage > Random.Range(0, 100))
            {
                return true;
            }
            return false;
        }

        public Status GetStatus(Status.StatusType statusType, int statusDuration, HoverUI hover)
        {
            Status status = SimplePool.Spawn(StatusPrefab).GetComponent<Status>();
            status.transform.SetParent(hover.StatusContainer);
            status.Assign(statusSprites[(int)statusType], statusType, statusDuration);
            return status;
        }

        private void ApplyStatus(Entity entity, Status.StatusType statusType, int statusDuration, int inflictChance)
        {
            if (statusType != Status.StatusType.None &&
                statusType != Status.StatusType.Dispel &&
                statusType != Status.StatusType.IgnoreDef)
            {
                if (inflictChance > Random.Range(0, 100))
                {
                    entity.AddStatus(GetStatus(statusType, statusDuration, entity.HoverGroup));
                }
            }
        }

        #endregion


        #region --- Game State Conditions ---

        private void CheckVictory()
        {
            for (int i = 0; i < enemyEntities.Count; i++)
            {
                if (!enemyEntities[i].IsDead())
                {
                    return;
                }
            }
            Timing.RunCoroutine(_Victory());
        }
        private IEnumerator<float> _Victory()
        {
            yield return Timing.WaitForSeconds(1f);
            EndBattle();
        }

        private void CheckDefeat()
        {
            for (int i = 0; i < allyEntities.Count; i++)
            {
                if (!allyEntities[i].IsDead())
                {
                    return;
                }
            }
        }
        private IEnumerator<float> _Defeat()
        {
            yield return Timing.WaitForSeconds(1f);
            MainManager.instance.IsPlaying = false;
            EndBattle();
        }

        #endregion


        public Entity GetRandomEnemy()
        {
            return enemyEntities[Random.Range(0, enemyEntities.Count)];
        }

        public Entity GetRandomAlly()
        {
            return allyEntities[Random.Range(0, allyEntities.Count)];
        }

        private void Think()
        {
            Skill chosenSkill = CurrentEntity.GetRandomSkill();
            CurrentEntity.CurrentMP -= chosenSkill.Cost * 3;
            switch (chosenSkill.Targeting)
            {
                case Skill.Target.Self:
                    PerformSkill(CurrentEntity, CurrentEntity, chosenSkill);
                    break;
                case Skill.Target.Ally:
                    PerformSkill(CurrentEntity, GetRandomEnemy(), chosenSkill);
                    break;
                case Skill.Target.AllAllies:
                    for (int i = 0; i < enemyEntities.Count; i++)
                    {
                        PerformSkill(CurrentEntity, enemyEntities[i], chosenSkill);
                    }
                    break;
                case Skill.Target.Enemy:
                    PerformSkill(CurrentEntity, GetRandomAlly(), chosenSkill);
                    break;
                case Skill.Target.AllEnemies:
                    for (int i = 0; i < allyEntities.Count; i++)
                    {
                        PerformSkill(CurrentEntity, allyEntities[i], chosenSkill);
                    }
                    break;
            }
        }

        private Vector2 TrackPosition(float progress)
        {
            return new Vector2((Mathf.Clamp(progress, 0, 100) / 100) * 450 - 50f, 0);
        }
    }
}
