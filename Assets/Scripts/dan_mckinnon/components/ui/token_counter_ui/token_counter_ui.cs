﻿// Dan's Jam Pack for Unity #1.
// ---
// by Dan Player McKinnon
// E-mail: dan@danmckinnon.net
// Created: February 2016
// Description:
//		This asset pack gives some miscellanous boilerplate functionality
//		that are very handy when prototyping and jamming out games.
// Liscence:  
//		This work is licensed under the Creative Commons Attribution 4.0 International License. 
//		To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/.

// UI Token Counter
// ---
// A generic health / energy counter that displays a row of sprites on the UI.  
// Note: requires two sprites representing an 'on' and an 'off' state.
// keeps track of not only a counter but the maximum value of the stats being tracked.
//
// NOTE: values can exceed the maximum and minimum values.
//	if a the amount exceeds the max,  extra counters will be applied
//	if the maximum is less than or equal to 0 the 'off' sprites will not be used.

using UnityEngine;
using System.Collections;
[AddComponentMenu("Dan McKinnon/UI/Token counter")]
public class token_counter_ui : MonoBehaviour {
    public enum axis_type { x, y }
    [Tooltip("Which direction the sprites are displayed.")]
    public axis_type axis = axis_type.x;
    [Tooltip("The initial amount of ")]
    public int start_amount = 5;
    [Tooltip("The initial maximum")]
    public int start_max = 8;
    [Tooltip("A sprite representing an on state. ")]
    public GameObject on_image;
    [Tooltip("A sprite representing the off state.")]
    public GameObject off_image;
    [Tooltip("The distance between sprites in UI space.")]
    public float spacing;
    [Tooltip("The vertical and horizontal padding of the counter in UI space.")]
    public float padding = 64;

    public void clear() {
        foreach( Transform child in transform) {
            Destroy(child.gameObject);
        }
    }

    private int last_amount = 0;
    private int last_max = 0;

    
    //  set_amount( count, max)
    // ---
    //	count	- the number of health/energy/etc the player has
    //	max		- the maximum health/energy/etc the player can have
    public void set_amount( int count, int max ) {
        if (!(count == last_amount && max == last_max)) {
            last_amount = count;
            last_max = max;
            clear();
            int i = 0;

            RectTransform rt = GetComponent<RectTransform>();
            float xx = rt.rect.xMin + padding;
            float yy = rt.rect.yMin + padding;
            float x_inc = 0;
            float y_inc = 0;
            if ( axis == axis_type.x) {
                x_inc = spacing;
            } else {
                y_inc = spacing;
            }
            GameObject child;
            for (i = 0; i < count; i++) {
                child = (GameObject)Instantiate(on_image,  new Vector3(xx,yy, 0), Quaternion.identity);
                //child.AddComponent<RectTransform>();
                child.transform.SetParent(gameObject.transform,false);
                xx += x_inc;
                yy += y_inc;
            }
            for (; i < max; i++) {
                child = (GameObject)Instantiate(off_image, new Vector3(xx, yy, 0), Quaternion.identity);
                //child.AddComponent<RectTransform>();
                child.transform.SetParent(gameObject.transform, false);
                xx += x_inc;
                yy += y_inc;

            }
        }
    }

    //  add_max( i )
    // ---
    //	i		- add i to the maximum amount
    public void add_max(int i = 1) {
        set_amount(last_amount, last_max + i);

    }
    //  lower_max(i)
    // ---
    //	i		- remove from the maximum amount
    public void lower_max(int i =1) {
        set_amount(last_amount, last_max - i);

    }

    // inc( i )
    // ---
    //	i		- # to add to the counter
    //			- if not specified = 1
    public void inc(int i = 1) {
        set_amount(last_amount + i, last_max);
    }

    // dec(i)
    // ---
    //	i		- # to remove from the counter
    public void dec(int i = 1) {
        set_amount(last_amount - i, last_max);

    }
    
    // Use this for initialization
    void Start () {
        set_amount(start_amount, start_max);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
