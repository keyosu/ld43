﻿// Dan's Jam Pack for Unity #1.
// ---
// by Dan Player McKinnon
// E-mail: dan@danmckinnon.net
// Created: February 2016
// Description:
//		This asset pack gives some miscellanous boilerplate functionality
//		that are very handy when prototyping and jamming out games.
// Liscence:  
//		This work is licensed under the Creative Commons Attribution 4.0 International License. 
//		To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/.

// Follow object
// ---
// Make your camera follow a specific object rigidly.   
// The camera will never leave the boundaries defined even if the object it is following does.
using UnityEngine;
using System.Collections;

[AddComponentMenu("Dan McKinnon/Cameras/Follow object")]
public class follow_object : MonoBehaviour {
    [Tooltip("The offset distance in which to follow.  Set this to 0,0 if you want the object to stay in the very center of the camera.  ")]
    public Vector2 offset = Vector3.zero;
    [Tooltip("The bounadaries in which the camera will not escape")]
    public Rect boundaries = new Rect(-10, -10, 20, 20);               //a rectangular confined boundaries where the camera can go.

    [Tooltip("The object to follow")]
    public GameObject follow_what;
    private Transform follow;
    private Camera cam;

	// Use this for initialization
	void Start () {
        if (follow_what) {
            follow = follow_what.GetComponent<Transform>();
        } else {
            enabled = false;
            Debug.LogWarning("Follow camera component requires an object to follow.");
        }
        cam = GetComponent<Camera>();
        
	}
	
	// Update is called once per frame
	void Update () {
        if (follow_what ) {
            float w = (cam.orthographicSize / cam.pixelHeight) * cam.pixelWidth;
            transform.position = new Vector3(Mathf.Clamp(follow.position.x, boundaries.xMin + w, boundaries.xMax - w), Mathf.Clamp(follow.position.y, boundaries.yMin + cam.orthographicSize, boundaries.yMax - cam.orthographicSize), 0);
        }
        
	}

    void OnDrawGizmosSelected() {
        Vector3 top_left = new Vector3(boundaries.xMin, boundaries.yMin, transform.position.z);
        Vector3 top_right = new Vector3(boundaries.xMax, boundaries.yMin, transform.position.z);
        Vector3 bottom_left = new Vector3(boundaries.xMin, boundaries.yMax, transform.position.z);
        Vector3 bottom_right = new Vector3(boundaries.xMax, boundaries.yMax, transform.position.z);

        Gizmos.color = Color.red;
        Gizmos.DrawLine(top_left, top_right);
        Gizmos.DrawLine(top_right, bottom_right);
        Gizmos.DrawLine(bottom_right, bottom_left);
        Gizmos.DrawLine(bottom_left, top_left);

        Gizmos.color = Color.green;
        Gizmos.DrawIcon(transform.position + new Vector3(offset.x, offset.y, 0), "eye.png");
    }
}
