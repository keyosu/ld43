﻿// Dan's Jam Pack for Unity #1.
// ---
// by Dan Player McKinnon
// E-mail: dan@danmckinnon.net
// Created: February 2016
// Description:
//		This asset pack gives some miscellanous boilerplate functionality
//		that are very handy when prototyping and jamming out games.
// Liscence:  
//		This work is licensed under the Creative Commons Attribution 4.0 International License. 
//		To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/.

// Persistent menu item
// ---
// Make a game object persistent or create a new one that is persistent.
//
using UnityEngine;
using System.Collections;
using UnityEditor;

public class persistent_menu_item{
    //
    // ---
    //
    //
	[MenuItem("GameObject/Dan McKinnon/Misc/Persistent group",false,10)]
	public static void add_persistent_group(){

		if ( Selection.activeGameObject ){
			bool answer = EditorUtility.DisplayDialog("Attach component instead","Would you like to apply persistence to the current GameObject instead?","Yes","No");
			if ( answer ){
				Undo.AddComponent<persistent>(Selection.activeGameObject);
				return;
			}

		}


		GameObject o = new GameObject("persistent");
		o.AddComponent<persistent>();
		if ( Selection.activeGameObject ){
			o.transform.SetParent(Selection.activeGameObject.transform );			
		}
		Undo.RegisterCreatedObjectUndo(o,"create persistent group");
	}

}
