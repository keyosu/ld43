﻿// Dan's Jam Pack for Unity #1.
// ---
// by Dan Player McKinnon
// E-mail: dan@danmckinnon.net
// Created: February 2016
// Description:
//		This asset pack gives some miscellanous boilerplate functionality
//		that are very handy when prototyping and jamming out games.
// Liscence:  
//		This work is licensed under the Creative Commons Attribution 4.0 International License. 
//		To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/.

// Level changer menu item
// ---
// Add an empty game Object with a level changer
//
using UnityEngine;
using System.Collections;
using UnityEditor;

public class level_changer_menu_item{
    //
    // ---
    //
    //
	[MenuItem("GameObject/Dan McKinnon/Misc/Level changer",false,10)]
	public static void add_level_changer(){
		GameObject o = new GameObject("level_changer");
		o.AddComponent<level_changer>();
		if ( Selection.activeGameObject ){
			o.transform.SetParent(Selection.activeGameObject.transform );			
		}
		Undo.RegisterCreatedObjectUndo(o,"create level changer");
	}

}
