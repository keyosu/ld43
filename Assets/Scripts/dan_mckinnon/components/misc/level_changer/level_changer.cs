﻿// Dan's Jam Pack for Unity #1.
// ---
// by Dan Player McKinnon
// E-mail: dan@danmckinnon.net
// Created: February 2016
// Description:
//		This asset pack gives some miscellanous boilerplate functionality
//		that are very handy when prototyping and jamming out games.
// Liscence:  
//		This work is licensed under the Creative Commons Attribution 4.0 International License. 
//		To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/.

// Level changer
// ---
// Add level-changing functionality.  

using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
[AddComponentMenu("Dan McKinnon/Misc/Level changer")]
public class level_changer : MonoBehaviour {

    [HideInInspector]
    public int current_level = 1;

    void Start() {
        current_level = SceneManager.GetActiveScene().buildIndex;
    }
    
    // 
    // ---
    //
    public void next_level() {
        int id = SceneManager.GetActiveScene().buildIndex + 1;
        id = id < SceneManager.sceneCountInBuildSettings ? id : 1;
        current_level = id;
        SceneManager.LoadScene(id);
    }

    // 
    // ---
    //
    public void last_level() {
        int id = SceneManager.GetActiveScene().buildIndex - 1;
        id = id > 0 ? id : SceneManager.sceneCountInBuildSettings;
        current_level = id;
        SceneManager.LoadScene(id);
    }

    // 
    // ---
    //
    public void go_to_level(int id) {
        SceneManager.LoadScene(id);
    }

    // 
    // ---
    //
    public void go_to_level(string name) {
        SceneManager.LoadScene(name);
    }
}
