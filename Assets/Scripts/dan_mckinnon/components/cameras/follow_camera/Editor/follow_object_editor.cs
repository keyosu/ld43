﻿// Dan's Jam Pack for Unity #1.
// ---
// by Dan Player McKinnon
// E-mail: dan@danmckinnon.net
// Created: February 2016
// Description:
//		This asset pack gives some miscellanous boilerplate functionality
//		that are very handy when prototyping and jamming out games.
// Liscence:  
//		This work is licensed under the Creative Commons Attribution 4.0 International License. 
//		To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/.

// Follow Object Editor
// ---
// Edit the boundaries and offset of a follow object camera.
using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(follow_object))]
public class follow_object_editor : Editor {
    void OnSceneGUI() {
        follow_object t = (follow_object)target;

        EditorGUI.BeginChangeCheck();
        Vector3 bottom_left = new Vector3(t.boundaries.xMin, t.boundaries.yMin, t.transform.position.z);
        Vector3 top_right = new Vector3(t.boundaries.xMax, t.boundaries.yMax, t.transform.position.z);
        Vector3 offset = t.transform.position + new Vector3(t.offset.x, t.offset.y, 0);


        bottom_left = Handles.PositionHandle(bottom_left, Quaternion.identity);
        top_right = Handles.PositionHandle(top_right, Quaternion.identity);
        offset = Handles.PositionHandle(offset, Quaternion.identity);

        if (EditorGUI.EndChangeCheck()) {
            Undo.RegisterCompleteObjectUndo(t, "modify camera boundaries");

            Vector3 size = top_right - bottom_left;
            t.boundaries = new Rect(bottom_left.x, bottom_left.y, size.x, size.y);

            t.offset = offset - t.transform.position;
        }
    }
}
