﻿// Dan's Jam Pack for Unity #1.
// ---
// by Dan Player McKinnon
// E-mail: dan@danmckinnon.net
// Created: February 2016
// Description:
//		This asset pack gives some miscellanous boilerplate functionality
//		that are very handy when prototyping and jamming out games.
// Liscence:  
//		This work is licensed under the Creative Commons Attribution 4.0 International License. 
//		To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/.

// Sound on collide
// ---
// Plays an audio source upon collision.
// Requires a 2D collider attached to the game object as well.

using UnityEngine;
using System.Collections;

[AddComponentMenu("Dan McKinnon/Sound/Sound on collide")]
public class sound_on_collide : MonoBehaviour {

	[Tooltip("")]
    public GameObject sound_source;
    void OnCollisionEnter2D(Collision2D c) {
        AudioSource clip = sound_source.GetComponent<AudioSource>();
        //clip.Play();
        clip.PlayOneShot(clip.clip);

    }
}
