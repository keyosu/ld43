﻿// Dan's Jam Pack for Unity #1.
// ---
// by Dan Player McKinnon
// E-mail: dan@danmckinnon.net
// Created: February 2016
// Description:
//		This asset pack gives some miscellanous boilerplate functionality
//		that are very handy when prototyping and jamming out games.
// Liscence:  
//		This work is licensed under the Creative Commons Attribution 4.0 International License. 
//		To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/.

// Spawner Menu
// ---
// Conveniently add a spawner  OR convert an object into a spawner that spawns that object.
//
using UnityEngine;
using System.Collections;
using UnityEditor;

public class spawner_menu_item {
    //
    // ---
    //
    //
    [MenuItem("GameObject/Dan McKinnon/Physics/Loop spawner",false,10)]
    public static void add_spawner() {
        GameObject o = new GameObject("loop_spawner");
        if ( Selection.activeGameObject) {
            o.transform.SetParent(Selection.activeGameObject.transform);
        }
        o.AddComponent<spawn_loop>();
        Undo.RegisterCreatedObjectUndo(o, "create spawner loop");
    }


    //
    // ---
    //
    //
    //puts the currently selected item into a spawner, disable it,  then link it to the spawner.
    [MenuItem("GameObject/Dan McKinnon/Physics/Convert to loop spawner",false,10)]
    public static void convert_to_spawner() {
        //todo:  add original arrangement to undo somehow...
        
        if ( Selection.activeGameObject ){
            GameObject from = Selection.activeGameObject;
            GameObject o = new GameObject("loop_spawner");
            o.transform.position = from.transform.position;
            Transform t = from.transform.parent;
            spawn_loop sp = o.AddComponent<spawn_loop>();
            sp.clone_what = from;
            Selection.activeGameObject.transform.SetParent(o.transform);
            o.transform.SetParent(t);
            from.SetActive(false);

        } else {
            EditorUtility.DisplayDialog("Select game object","You must select a game object to turn into a spawner.","Ok");

        }
    }



}
