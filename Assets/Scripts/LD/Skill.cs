﻿using Sirenix.OdinInspector;
using UnityEngine;

namespace LD
{
    [System.Serializable]
    public class Skill : ScriptableObject
    {
        public enum Target
        {
            Enemy, AllEnemies, Ally, AllAllies, Self
        }

        [ReadOnly]
        public string Name;
        public Sprite Icon;
        [TextArea(4, 6)]
        public string Description;

        [Space(5)]
        public bool IsMagic = false;
        public bool IsRecovery = false;
        public int Power = 100;
        public int Accuracy = 100;
        public int Cost = 0;
        public float Rating = 0;

        [Space(5)]
        public Target Targeting;
        public Status.StatusType TargetStatus;
        [ShowIf("HasTargetStatus")]
        public int TargetDuration = 1;
        [ShowIf("HasTargetStatus")]
        public int TargetPercentage = 100;

        public Status.StatusType UserStatus;
        [ShowIf("HasUserStatus")]
        public int UserDuration = 1;
        [ShowIf("HasUserStatus")]
        public int UserPercentage = 100;

        public void OnValidate()
        {
            Name = name;
        }

        [Button]
        public void CalculateRating()
        {
            Rating = (Power * 2 + Accuracy + Cost * 150 + StatusRating * 500) / 900f;
        }

        public string ParsedDescription
        {
            get
            {
                string text = Description + "<size=10>";
                if (Description.Contains("damage"))
                {
                    text += "\nPower: " + Power.ToString();
                }
                if (Description.Contains("[Guard]"))
                {
                    text += "\nGuard: Take 50% less damage.";
                }
                if (Description.Contains("[Provoke]"))
                {
                    text += "\nProvoke: Uncontrollable. Will always attack a random target.";
                }
                if (Description.Contains("[Def Down]"))
                {
                    text += "\nDef Down: Defence reduced by 50%.";
                }
                if (Description.Contains("[Def Up]"))
                {
                    text += "\nDef Up: Defence increased by 50%.";
                }
                if (Description.Contains("[Atk Down]"))
                {
                    text += "\nAtk Down: Attack reduced by 50%.";
                }
                if (Description.Contains("[Atk Up]"))
                {
                    text += "\nAtk Up: Attack increased by 50%.";
                }
                if (Description.Contains("[Spd Down]"))
                {
                    text += "\nSpd Down: Speed reduced by 50%.";
                }
                if (Description.Contains("[Spd Up]"))
                {
                    text += "\nSpd Up: Speed increased by 50%.";
                }
                if (Description.Contains("[Dispel]"))
                {
                    text += "\nDispel: Removes a buff.";
                }
                if (Description.Contains("[Stun]"))
                {
                    text += "\nStun: Cannot take an action.";
                }
                if (Description.Contains("[Drain]"))
                {
                    text += "\nDrain: Gain 50% of damage dealt as HP.";
                }
                return text;
            }
        }

        private float StatusRating
        {
            get
            {
                return (GetRating(TargetStatus, TargetDuration) + GetRating(UserStatus, UserDuration)) * 100;
            }
        }

        private float GetRating(Status.StatusType type, int duration)
        {
            float baseRating = 0;
            switch (type)
            {
                case Status.StatusType.Guard:
                case Status.StatusType.Bleed:
                case Status.StatusType.Dispel:
                    baseRating = 0.5f;
                    break;

                case Status.StatusType.Provoke:
                case Status.StatusType.Drain:
                case Status.StatusType.DefDown:
                case Status.StatusType.DefUp:
                case Status.StatusType.AtkDown:
                case Status.StatusType.AtkUp:
                case Status.StatusType.SpdUp:
                case Status.StatusType.SpdDown:
                    baseRating = 0.3f;
                    break;

                case Status.StatusType.Stun:
                case Status.StatusType.IgnoreDef:
                case Status.StatusType.Stack:
                    baseRating = 0.75f;
                    break;
            }
            return baseRating * duration;
        }

        private bool HasUserStatus()
        {
            return UserStatus != Status.StatusType.None;
        }

        private bool HasTargetStatus()
        {
            return TargetStatus != Status.StatusType.None;
        }
    }
}
