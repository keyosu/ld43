﻿// Dan's Jam Pack for Unity #1.
// ---
// by Dan Player McKinnon
// E-mail: dan@danmckinnon.net
// Created: February 2016
// Description:
//		This asset pack gives some miscellanous boilerplate functionality
//		that are very handy when prototyping and jamming out games.
// Liscence:  
//		This work is licensed under the Creative Commons Attribution 4.0 International License. 
//		To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/.

// Follow camera menu
// ---
// Conveniently add a follow camera.
//
using UnityEngine;
using System.Collections;
using UnityEditor;

public class follow_camera_gameobject_menu {
    //
    // ---
    //
    //
    [MenuItem("GameObject/Dan McKinnon/Cameras/Follow camera 2D", false, 10)]
    public static void follow_camera_2d() {

        GameObject o;
        string name = "follow_camera";
        if ( Camera.main) {
            bool answer = EditorUtility.DisplayDialog("Attach component", "Attach follow component to main camera?", "Yes", "No");
            if ( answer) {
                o = Camera.main.gameObject;

                if (!Camera.main.orthographic) {
                    bool answer2 = EditorUtility.DisplayDialog("Change perspective", "This component requires a 2D orthographic projection to work correctly.  Change perspective now?", "Yes", "No");
                    if (answer2) {
                        Camera.main.orthographic = true;
                        Camera.main.orthographicSize = 4;
                    }
                }
                if (o.GetComponent<follow_object>()) {
                    EditorUtility.DisplayDialog("Follow camera already attached", "A follow camera component was already attached to the main camera.", "Ok");
                } else {

                    Undo.AddComponent<follow_object>(o);
                    
                }
                return;
            }
        }

        o = new GameObject(name);
        if (!Camera.main) {
            o.tag = "MainCamera";
        }

        o.AddComponent<Camera>();
        Camera cam = o.GetComponent<Camera>();
        cam.orthographic = true;
        cam.orthographicSize = 4;

        o.AddComponent<AudioListener>();
        o.AddComponent<GUILayer>();
        o.AddComponent<follow_object>();
        Undo.RegisterCreatedObjectUndo(o,"create follow camera");
    }

}