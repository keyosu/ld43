﻿using MEC;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace LD
{
    public class HoverUI : MonoBehaviour
    {
        public bool UseCachedPosition = true;
        public RectTransform StatusContainer;

        [Space(5)]
        public Image HealthBar;
        public TextMeshProUGUI HealthValue;
        [Space(5)]
        public Image ManaBar;
        public TextMeshProUGUI ManaValue;
        [Space(5)]
        public TextMeshProUGUI OtherValue;

        [Space(5)]
        public Entity Owner;

        private Vector2 lastPosition;
        private RectTransform rectTransform;

        public void Assign(Entity entity)
        {
            Owner = entity;
            gameObject.SetActive(true);

            if (rectTransform == null)
            {
                rectTransform = GetComponent<RectTransform>();
            }
        }

        public void Unassign()
        {
            SimplePool.Despawn(gameObject);
        }

        public void Update()
        {
            if (Owner != null && (!UseCachedPosition || lastPosition != (Vector2)Owner.transform.position))
            {
                lastPosition = Owner.transform.position;

                Vector2 viewport = Camera.main.WorldToViewportPoint(Owner.transform.position + (Vector3)Owner.Offset);
                rectTransform.anchoredPosition = Vector2.right * viewport.x * Screen.width + Vector2.up * viewport.y * Screen.height;
            }
        }

        public void UpdateHP(int currentHP, int maxHP)
        {
            HealthValue.text = currentHP.ToString() + "/" + maxHP.ToString();
            HealthBar.fillAmount = currentHP * 1f / maxHP;
        }

        public void UpdateMP(int currentMP, int maxMP)
        {
            ManaValue.text = currentMP.ToString() + "/" + maxMP.ToString();
            ManaBar.fillAmount = currentMP * 1f / maxMP;
        }

        public void SetLifetime(float lifetime)
        {
            Timing.RunCoroutine(_Unassign(lifetime));
        }
        private IEnumerator<float> _Unassign(float delay)
        {
            yield return Timing.WaitForSeconds(delay);
            Unassign();
        }
    }
}