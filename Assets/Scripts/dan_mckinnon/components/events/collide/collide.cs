﻿// Dan's Jam Pack for Unity #1.
// ---
// by Dan Player McKinnon
// E-mail: dan@danmckinnon.net
// Created: February 2016
// Description:
//		This asset pack gives some miscellanous boilerplate functionality
//		that are very handy when prototyping and jamming out games.
// Liscence:  
//		This work is licensed under the Creative Commons Attribution 4.0 International License. 
//		To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/.

// Collide
// ---
// Assign a unity event upon collision
// Requires a collider (2D or 3D).
using UnityEngine;
using System.Collections;
using UnityEngine.Events;
[AddComponentMenu("Dan McKinnon/Events/On collide")]
public class collide : MonoBehaviour {

    [Tooltip("Triggered when something collides with this object")]
    public UnityEvent on_collide;

    void OnCollisionEnter2D(Collision2D c) {
        on_collide.Invoke();
    }
    void OnCollisionEnter(Collision c) {
        on_collide.Invoke();

    }
}
