﻿using DG.Tweening;
using MEC;
using Sirenix.OdinInspector;
using System.Collections.Generic;
using UnityEngine;

namespace LD
{
    public class Entity : MonoBehaviour
    {
        public Animator m_Animator;
        public Sprite Icon;
        public bool IsPlayer;
        public Vector2 Offset = Vector2.up * 1.2f;
        public Vector2 EndPosition;

        [Space(10)]
        public int LVL = 1;

        [ReadOnly]
        public int StatPoints;

        [Space(5)]
        public int MaxHP;
        public int MaxMP;

        [Space(5)]
        public int STR = 1;
        public int INT = 1;
        public int VIT = 1;
        public int AGI = 1;
        public int DEX = 1;

        [Space(10)]
        [SerializeField]
        private Equip weapon;
        [SerializeField]
        private Equip armor;
        [SerializeField]
        private Equip accessory;

        [Space(5)]
        public RectTransform TurnTrack;
        public HoverUI HoverGroup;

        [Space(10)]
        public List<Skill> skills = new List<Skill>();

        [Space(10)]
        public bool InAnimation;
        public float TurnProgress;
        public List<Status> Statuses = new List<Status>();

        public AudioClip AudioLevelUp;

        private Vector3 startPosition;
        private int currentHP;
        private int currentMP;
        private int currentExp;

        public float PATK
        {
            get
            {
                float modifier = 1f;
                if (HasStatus(Status.StatusType.AtkUp))
                {
                    modifier = 1.25f;
                }
                else if (HasStatus(Status.StatusType.AtkDown))
                {
                    modifier = 0.75f;
                }
                return (LVL * 5f + STR * 2.1f) * modifier;
            }
        }

        public float MATK
        {
            get
            {
                float modifier = 1f;
                if (HasStatus(Status.StatusType.AtkUp))
                {
                    modifier = 1.25f;
                }
                else if (HasStatus(Status.StatusType.AtkDown))
                {
                    modifier = 0.75f;
                }
                return (LVL * 5f + INT * 2.1f) * modifier;
            }
        }

        public float PDEF
        {
            get
            {
                float modifier = 1f;
                if (HasStatus(Status.StatusType.DefUp))
                {
                    modifier = 1.25f;
                }
                else if (HasStatus(Status.StatusType.DefDown))
                {
                    modifier = 0.75f;
                }
                return (LVL * 3f + VIT * 1.2f + STR * 0.5f) * modifier;
            }
        }

        public float MDEF
        {
            get
            {
                float modifier = 1f;
                if (HasStatus(Status.StatusType.DefUp))
                {
                    modifier = 1.25f;
                }
                else if (HasStatus(Status.StatusType.DefDown))
                {
                    modifier = 0.75f;
                }
                return (LVL * 3f + VIT * 1.2f + INT * 0.5f) * modifier;
            }
        }

        public float ACCURACY
        {
            get
            {
                return LVL * 5f + DEX * 2.1f + AGI * 0.5f;
            }
        }

        public float EVASION
        {
            get
            {
                return LVL * 3f + AGI * 2.1f + DEX * 0.5f;
            }
        }

        public float SPEED
        {
            get
            {
                float modifier = 1f;
                if (HasStatus(Status.StatusType.SpdUp))
                {
                    modifier = 1.25f;
                }
                else if (HasStatus(Status.StatusType.SpdDown))
                {
                    modifier = 0.75f;
                }
                return (LVL * 1.1f + (DEX + AGI) * 2.2f) * modifier;
            }
        }

        public float CRITICAL
        {
            get
            {
                return LVL * 3f + STR * 0.1f + INT * 0.1f + VIT * 0.3f + AGI * 0.15f + DEX * 0.15f;
            }
        }

        public int CurrentExp
        {
            get
            {
                return currentExp;
            }
            set
            {
                currentExp = value;
                if (currentExp > RequiredExp)
                {
                    currentExp -= RequiredExp;
                    LVL += 1;
                    PlayMusic.instance.PlaySFX(AudioLevelUp);
                    PopulateStats();
                    CalculateStats();
                    ResetStats();
                }
            }
        }

        public int RequiredExp
        {
            get
            {
                return 50 + LVL * 30;
            }
        }

        public int CurrentHP
        {
            get
            {
                return currentHP;
            }
            set
            {
                currentHP = value;
                if (currentHP < 0)
                {
                    currentHP = 0;
                }
                if (currentHP > MaxHP)
                {
                    currentHP = MaxHP;
                }
                HoverGroup.UpdateHP(currentHP, MaxHP);
            }
        }

        public int CurrentMP
        {
            get
            {
                return currentMP;
            }
            set
            {
                currentMP = value;
                if (currentMP < 0)
                {
                    currentMP = 0;
                }
                if (currentMP > MaxMP)
                {
                    currentMP = MaxMP;
                }
                HoverGroup.UpdateMP(currentMP, MaxMP);
            }
        }

        public bool IsGuarding
        {
            get
            {
                return HasStatus(Status.StatusType.Guard);
            }
        }

        public bool IsDead()
        {
            return currentHP == 0;
        }

        public bool HasStatus(Status.StatusType type)
        {
            for (int i = 0; i < Statuses.Count; i++)
            {
                if (Statuses[i].statusType == type)
                {
                    return true;
                }
            }
            return false;
        }


        #region --- Stats ---

        [Button]
        public void CalculateStats()
        {
            StatPoints = 6 + LVL * 4 - STR - INT - VIT - AGI - DEX;
            MaxHP = Mathf.RoundToInt(LVL * 6.4f + VIT * 4.6f);
            MaxMP = Mathf.RoundToInt(LVL * 5.4f + INT * 3.6f);
        }

        public void PopulateStats()
        {
            CalculateStats();
            while (StatPoints > 0)
            {
                int rng = Random.Range(0, 5);
                switch (rng)
                {
                    case 0:
                        STR += 1;
                        break;
                    case 1:
                        INT += 1;
                        break;
                    case 2:
                        VIT += 1;
                        break;
                    case 3:
                        AGI += 1;
                        break;
                    case 4:
                        DEX += 1;
                        break;
                }
                CalculateStats();
            }
        }

        public void PopulateSkills(float budget)
        {
            int checkCount = 10;
            while (budget > 0 && checkCount > 0)
            {
                Skill targetSkill = MainManager.instance.AllSkills[Random.Range(0, MainManager.instance.AllSkills.Count)];
                if (budget >= targetSkill.Rating)
                {
                    skills.Add(targetSkill);
                    budget -= targetSkill.Rating;
                }
                else
                {
                    checkCount--;
                }
            }
        }

        private void ResetStats()
        {
            CurrentHP = MaxHP;
            CurrentMP = MaxMP;
        }

        #endregion


        #region --- Combat ---

        public void Init()
        {
            if (BattleManager.instance != null)
            {
                BattleManager.instance.Initialize(this);
                skills.Clear();
                skills.Add(BattleManager.instance.AttackSkill);
                skills.Add(BattleManager.instance.GuardSkill);

                CalculateStats();
                ResetStats();
            }
        }

        public void UpdateTurn()
        {
            TurnProgress -= 100;
            UpdateStatuses();
        }

        public void AddStatus(Status status)
        {
            Statuses.Add(status);
        }

        public Skill GetRandomSkill()
        {
            Skill s = null;
            while (s == null || s.Cost * 3 > CurrentMP)
            {
                s = skills[Random.Range(0, skills.Count)];
            }
            return s;
        }

        public bool UpdateTurnTrack()
        {
            TurnProgress += SPEED;
            if (TurnProgress > 100)
            {
                return true;
            }
            return false;
        }

        public void Death()
        {
            for (int i = 0; i < Statuses.Count; i++)
            {
                Statuses[i].End();
            }
            Statuses.Clear();

            Timing.RunCoroutine(_Death());
        }
        private IEnumerator<float> _Death()
        {
            if (IsPlayer)
            {
                m_Animator.SetTrigger("Death");
                yield return Timing.WaitForSeconds(0.5f);
            }
            yield return Timing.WaitForSeconds(0.5f);
            TurnTrack.gameObject.SetActive(false);
            gameObject.SetActive(false);
            HoverGroup.Unassign();
        }

        public void AnimationSkill(Entity target, Skill skill)
        {
            InAnimation = true;
            Timing.RunCoroutine(_AnimationSkill(target, skill));
        }
        private IEnumerator<float> _AnimationSkill(Entity target, Skill skill)
        {
            Vector3 startPosition = transform.position;
            transform.DOJump(target.transform.position, 1f, 1, 0.5f);
            InAnimation = false;
            // do some different animation based on skill?

            yield return Timing.WaitForSeconds(0.5f);

            transform.DOMove(startPosition, 0.5f);
        }

        private void UpdateStatuses()
        {
            bool canAct = true;
            bool provoked = false;
            for (int i = 0; i < Statuses.Count; i++)
            {
                Statuses[i].Progress();
                if (Statuses[i].statusType == Status.StatusType.Stun)
                {
                    canAct = false;
                }
                else if (Statuses[i].statusType == Status.StatusType.Provoke)
                {
                    provoked = true;
                }
            }
            for (int i = 0; i < Statuses.Count; i++)
            {
                if (!Statuses[i].IsActive)
                {
                    Statuses.RemoveAt(i);
                    i--;
                }
            }
            if (!canAct)
            {
                BattleManager.instance.NextTurn();
            }
            else if (provoked)
            {
                BattleManager.instance.PerformSkill(this,
                    IsPlayer ? BattleManager.instance.GetRandomEnemy() : BattleManager.instance.GetRandomAlly(),
                    BattleManager.instance.AttackSkill);
            }
        }

        #endregion

        public void Awake()
        {
            startPosition = transform.localPosition;
            if (IsPlayer)
            {
                m_Animator = GetComponentInChildren<Animator>();
            }
        }

        public void Enter()
        {
            if (!IsPlayer)
            {
                transform.localPosition = new Vector3(startPosition.x, startPosition.y, 10);
                transform.DOJump(new Vector3(EndPosition.x, EndPosition.y, 10), 1f, 1, 0.33f).SetDelay(0.1f);
            }
        }

        public void OnMouseDown()
        {
            if (BattleManager.instance.TargetAlly && IsPlayer)
            {
                BattleManager.instance.SelectTarget(this);
            }
            else if (!IsPlayer)
            {
                BattleManager.instance.SelectTarget(this);
            }
        }

        private void OnValidate()
        {
            CalculateStats();
        }
    }
}