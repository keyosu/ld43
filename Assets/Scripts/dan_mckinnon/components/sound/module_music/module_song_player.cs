﻿// Dan's Jam Pack for Unity #1.
// ---
// by Dan Player McKinnon
// E-mail: dan@danmckinnon.net
// Created: February 2016
// Description:
//		This asset pack gives some miscellanous boilerplate functionality
//		that are very handy when prototyping and jamming out games.
// Liscence:  
//		This work is licensed under the Creative Commons Attribution 4.0 International License. 
//		To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/.

// Modular song player
// ---
// Used in conjunction with modular song.
// Unity has a few problem with playback of music:
//		1. There is a short pause on loop
//		2. You cannot loop to a position in the song.
//		3. Cannot reduce the song size even if the song has repetative parts.
//
// This component addresses the problems and adds the convience of being able to track what part of the song
// is currently playing.
//  See the help for modular song for setting up your songs.
//
// Note: This component works for layered musical content,  but requires two players and two songs to do so.
using UnityEngine;
using System.Collections;
using UnityEngine.Audio;
[AddComponentMenu("Dan McKinnon/Music/Modular music/Module player")]
public class module_song_player : MonoBehaviour {

    private module_song song;
    private int repeat_position = 0;
    private int position = 0;
    private int num_clips;
    private double next_clip_time;

    public int current_position
    {
        get
        {
            return tracked_position;
        }
        set
        {
            CancelInvoke("schedule_next_clip");
            CancelInvoke("update_tracked_position");
            position = value;
            tracked_position = value;
            schedule_next_clip();
        }
    }
    
    
    [HideInInspector]
    public bool playing = false;

    // Play( s )
    // ---
    //	s - A modular song to play
    // Plays the modular song specified,  stopping any previous song immediately.
    public void play( module_song s) {
        if (playing) {
            stop();
        }
        tracked_position = 0;
        position = 0;
        playing = true;
        double start_time = AudioSettings.dspTime + 0.1;
        next_clip_time = start_time;
        song = s;
        repeat_position = s.repeat_position;
        num_clips = song.transform.childCount;

        schedule_next_clip();


    }

    // stop()
    // ---
    // Stops any song playing immediately.
    public void stop() {
        //stop all invokes
        //stop all audio source children

        tracked_position = -1;
        CancelInvoke("schedule_next_clip");
        CancelInvoke("update_tracked_position");

        foreach ( Transform child in song.transform) {
            AudioSource src = child.GetComponent<AudioSource>();
            if ( src && src.isPlaying) {
                src.Stop();
            }
        }
    }
    private int tracked_position = 0;
    private void schedule_next_clip() {

        Transform next_song_transform = song.transform.GetChild(position);
        AudioSource src = next_song_transform.gameObject.GetComponent<AudioSource>();
        if (src != null) {
            src.PlayScheduled(next_clip_time);
            next_clip_time += src.clip.length;            
        }


        position++;
        if ( position >= num_clips) {
            position = repeat_position;
        }

        Invoke("schedule_next_clip", (float)(next_clip_time - AudioSettings.dspTime - 1));
        Invoke("update_tracked_position",(float)(next_clip_time - AudioSettings.dspTime));
        
    }
    private void update_tracked_position() {
        tracked_position++;
        if (tracked_position >= song.transform.childCount) {
            tracked_position = repeat_position;
        }
    }

    public void Start() {
        enabled = false;
    }


}
