﻿using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;

namespace LD
{
    public abstract class ButtonUI : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
    {
        public TextMeshProUGUI Title;

        public abstract void Setup();
        public abstract void OnPointerEnter(PointerEventData eventData);
        public abstract void OnPointerExit(PointerEventData eventData);
    }
}
