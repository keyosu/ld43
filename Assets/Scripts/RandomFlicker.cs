﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomFlicker : MonoBehaviour {


    public float minSize = 0.5f;
    public float maxSize = 1.5f;
    private float originalSize;
    [Range(0, 100)]
    public float flickerChance = 10f;
    private void Awake()
    {
        originalSize = transform.localScale.x;
    }
    void FixedUpdate () {
        if (Random.Range(0, 100) < flickerChance) {
            float r = Random.Range(minSize, maxSize);
            transform.localScale = Vector3.one * r;
        }
	}
}
