﻿using UnityEngine;
using System.Collections;

[AddComponentMenu("Dan McKinnon/Physics/Lifespan")]
public class lifespan : MonoBehaviour {

    public float minimum_lifespan;
    public float maximum_lifespan;
    private float end_time;

	// Use this for initialization
	void Start () {
        end_time = Time.time + Random.Range(minimum_lifespan, maximum_lifespan);
	}
	
	// Update is called once per frame
	void Update () {
	    if ( Time.time >  end_time) {
            Destroy(gameObject);
        }
	}
}
