﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(axis_mouse_tracker))]
public class axis_mouse_tracker_editor : Editor {
    void OnSceneGUI() {
        axis_mouse_tracker t = (axis_mouse_tracker)target;
        Vector3 min_pos;
        Vector3 max_pos;
        Vector3 pos;
        switch (t.axis) {
            case axis_type.x:
                EditorGUI.BeginChangeCheck();

                pos = new Vector3(t.min_val, t.transform.position.y, t.transform.position.z);
                min_pos = Handles.PositionHandle(pos, Quaternion.identity);
                Handles.Label(pos, "min");

                pos = new Vector3(t.max_val, t.transform.position.y, t.transform.position.z);
                max_pos = Handles.PositionHandle(pos, Quaternion.identity);
                Handles.Label(pos, "max");

                if (EditorGUI.EndChangeCheck()) {
                    t.min_val = min_pos.x;
                    t.max_val = max_pos.x;
                }
                break;
            case axis_type.y:
                EditorGUI.BeginChangeCheck();

                pos = new Vector3(t.transform.position.x, t.min_val, t.transform.position.z);
                min_pos = Handles.PositionHandle(pos, Quaternion.identity);
                Handles.Label(pos, "min");

                pos = new Vector3(t.transform.position.x, t.max_val, t.transform.position.z);
                max_pos = Handles.PositionHandle(pos, Quaternion.identity);
                Handles.Label(pos, "max");

                if (EditorGUI.EndChangeCheck()) {
                    t.min_val = min_pos.y;
                    t.max_val = max_pos.y;
                }
                break;


        }
    }

}
